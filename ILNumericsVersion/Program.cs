﻿using ILNumerics;
using System;
using System.Diagnostics;
using System.IO;
using static ILNumerics.ILMath;

namespace ConsoleApplication2 {
    /// <summary>
    /// A simple performance comparison between a trivial cpp algorithm and the same algorithm in ILNumerics. 
    /// </summary>
    class Program {
        const long length = 4000; 
        static void Main(string[] args) {
            // ILNumerics Version
            func();  // to eliminate JIT overhead
            time();  // do the actual timing: 100 iterations
                
            // Cpp version does all timing and 100 iterations
            var path = Path.Combine(Environment.CurrentDirectory, @"Cpp Version.exe"); 
            Process.Start(path);
            Console.Read(); 
        }

        static void time() {
            func();
            var sw = new Stopwatch();
            sw.Restart();
            for (int i = 0; i < 100; i++) {
                func();
            }
            sw.Stop();
            System.Console.WriteLine("ILNumerics Elapsed: " + sw.ElapsedMilliseconds);
        }
        /// <summary>
        /// This is the ILNumerics version: add two large matrices. 
        /// </summary>
        static void func() {
            using (Scope.Enter()) {
                Array<double> A = zeros(length, length),
                                B = ones(length, length), C;

                C = sum(A + B, 0); // sum along the columns
                //C = sum(A + B, 1); // this would sum up the rows
            }
        }
    }
}
