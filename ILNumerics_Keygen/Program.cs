﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using ILNumerics.Licensing;

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace ILNumerics_Keygen
{
    class Program
    {
        internal const string SAMPLE_LICENSE_FILE_NAME = "sample.lic.xml";
        internal const string PATCH_DLL_NAME = "ILNumerics.Core.dll";
        internal const string LICENSE_FILE_NAME = "ilnumerics.lic";

        [STAThread]
        static void Main(string[] args)
        {
            //随机生成一对公私钥
            //            RSACryptoServiceProvider sa = new RSACryptoServiceProvider(2048);
            //            var pub = sa.ToXmlString(false);
            //            var pri = sa.ToXmlString(true);

            //            var existsLicenses = RuntimeLicenseProvider.GetLicenses();
            //            if (existsLicenses.Any())
            //            {
            //                Console.WriteLine(@"Licenses已生效，无需进行任何处理");
            //                Application.EnableVisualStyles();
            //                Application.Run(new LicenseInfoForm());
            //                return;
            //            }

            //LoadResourcesFromFile("ILNumerics.IO.HDF5.ILNumerics_deploy.ilnumerics.lic");
            VerifyAssembly(@"D:\Program Files (x86)\ILNumerics\ILNumerics Ultimate VS\bin\ILNumerics.Drawing.dll");

            var machineKey = LicenseManager.GetMachineHash();
            var licenseFile = machineKey.Replace('/', '_') + ".lic";

            Console.WriteLine($@"请输入 {PATCH_DLL_NAME} 文件所在路径");
            var dll = Console.ReadLine();
            var dllDirectory = string.IsNullOrEmpty(dll) ? string.Empty : Path.GetDirectoryName(dll);
            if (File.Exists(dll))
            {
                var fileData = File.ReadAllBytes(dll);
                var fingerprintData = Encoding.Unicode.GetBytes(ILLicense.s_pubcont);
                var end = GetPositionAfterMatch(fileData, fingerprintData);
                var begin = end - fingerprintData.Length;
                if (begin < 0 || begin > fileData.Length - fingerprintData.Length)
                {
                    Console.WriteLine(@"未能在文件中找到特征码，跳过文件Patch");
                }
                else
                {
                    try
                    {
                        var keydata = Encoding.Unicode.GetBytes(ILLicense.PublicKey);
                        if (keydata.Length == fingerprintData.Length)
                        {
                            using var file = File.Open(dll, FileMode.Open, FileAccess.ReadWrite);
                            file.Position = begin;
                            file.Write(keydata, 0, keydata.Length);
                            Console.WriteLine(@"文件Patch已完成");
                        }
                        else Console.WriteLine(@"文件Patch无效，PublicKey长度不一致");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($@"文件Patch出现异常，详情：{ex.Message}");
                    }
                }
//                var publish= new System.EnterpriseServices.Internal.Publish(); 
//                publish.GacRemove(dll);
//                publish.GacInstall(dll);
            }
            else Console.WriteLine(@"路径不存在，跳过文件Patch");

            Console.WriteLine(@"将从注册表加载试用Licenses");

            if (!LicenseManager.IsILNumericsInstalled)
            {
                Console.WriteLine(@"请安装ILNumerics试用版后再运行本程序");
            }
            else
            {
                var trialLicenses = LoadLicenses();
                Console.WriteLine($@"已加载 {trialLicenses.Count.ToString()} 个Licenses，正在处理中请稍后");
                AdjustLicenses(trialLicenses,dllDirectory);
                List<Tuple<Guid, string>> preSavedLicenses = trialLicenses.Select(item => new Tuple<Guid, string>(item.ID, item.LicenseKey)).ToList();
                Console.WriteLine($@"正在更新 Licenses");
                LicenseManager.SetLicenses(preSavedLicenses);
                RebuildLicenseResources(dllDirectory, "ILNumerics.*.dll");
                SaveLicenseFile(trialLicenses, dllDirectory, licenseFile);
                Console.WriteLine($@"生成 Licenses 完成");
            }
            Console.WriteLine($@"以下位置的 {PATCH_DLL_NAME} 必须使用Patch后的dll替换才会生效
1.GAC
2. 安装目录\bin\
3.安装目录\Distribution Files\.NET Assemblies\
4.VS扩展目录
");
            Console.ReadKey();
        }

        static void RebuildLicenseResources(string dir,string filter)
        {
            if (Directory.Exists(dir))
            {
                var dlls = Directory.GetFiles(dir, filter);
              
                var assemblys = new List<Assembly>();
                foreach (var dll in dlls) assemblys.Add(Assembly.LoadFile(dll));
               

                if (LicenseManager.IsILNumericsInstalled)
                {
                    IEnumerable<ILDevelopmentLicense> enumerable  = LicenseManager.GetLicenses<ILDevelopmentLicense>(); ;
                    if (enumerable != null)
                    {
                        foreach (var assembly in assemblys)
                        {
                            var output = new object[2];
                            output[0] = Path.GetFileName(assembly.Location).ToUpper();
                            var hashtable = new Hashtable();
                         
                            AssemblyName[] array = (from r in assembly.GetReferencedAssemblies()
                                where r.Name.StartsWith("ILNumerics.")
                                select r).ToArray();

                            for (int i = 0; i < array.Length; i++)

                            {
                                AssemblyName r = array[i];
                                foreach (ILDevelopmentLicense ildevelopmentLicense in from devlic in enumerable
                                    where devlic.TargetAssemblySimpleName.Equals(r.Name)
                                    select devlic)
                                {
                                    ILAsmRuntimeLicense ilasmRuntimeLicense = new ILAsmRuntimeLicense(assembly, ildevelopmentLicense);
                                    var licenseKey = ilasmRuntimeLicense.ComputeKeyWithCh();
                                    hashtable.Add( $"{ilasmRuntimeLicense.TargetTypeName},{ilasmRuntimeLicense.AssemblyName}", licenseKey);

                                    Console.WriteLine(licenseKey);
                                }
                            }
                            output[1] = hashtable;
                            assembly.ManifestModule.GetPEKind(out var peKind, out var machine);
                            var data = BinarySerialize(output)
                                .Concat(Encoding.ASCII.GetBytes($"%%ILLicBuildConfig={(IsAssemblyDebugBuild(assembly)?"DEBUG":"RELEASE")}|AnyCPU%%{Environment.NewLine}"))
                                .ToArray();
                            File.WriteAllBytes(assembly.GetName().Name+ ".ILNumerics_deploy." + LICENSE_FILE_NAME,data);
                        }
                    }
                }
            }
            else Console.WriteLine(@"跳过ILNumerics程序集资源生成动作");
        }

        static void VerifyAssembly(string fileName)
        {
            var assembly = Assembly.LoadFile(fileName);
            LicenseManager.GetLicensesFromAssembly(assembly, out var licenses);
        }

        static bool IsAssemblyDebugBuild(Assembly assembly)
        {
            return assembly.GetCustomAttributes(false).OfType<DebuggableAttribute>().Any(da => da.IsJITTrackingEnabled);
        }

        static object[] LoadResourcesFromFile(string fileName)
        {
            if (!File.Exists(fileName)) return null;
            using var fileStream = File.OpenRead(fileName);
            object[] array = new BinaryFormatter().Deserialize(fileStream) as object[];
            return array;
        }

        static byte[] BinarySerialize(object obj)
        {
            using var ms = new MemoryStream();
            new BinaryFormatter().Serialize(ms, obj);
            return ms.ToArray();
        }

        static void SaveLicenseFile(List<ILLicense> licenses, string dirPath, string fileName)
        {
            var activationResponse = new ActivationResponse
            {
                Version = 1.0m,
                Content = licenses.Select(item => new License()
                {
                    GUID = item.ID.ToString("D").ToUpper(),
                    Value = Convert.ToBase64String(Encoding.Unicode.GetBytes(item.LicenseKey))
                }).ToArray()
            };
            if (string.IsNullOrEmpty(dirPath))
                dirPath = Environment.CurrentDirectory;
            //固定
            if (Directory.Exists(dirPath))
            {
                var path = Path.Combine(dirPath, fileName);
                if (File.Exists(path))
                {
                    Console.WriteLine($@"{path} 已存在，将备份至 {path}.bak");
                    try
                    {
                        File.Move(path, path + ".bak");
                    }
                    catch { }
                }
                var header = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                var sb = new StringBuilder();
                sb.Append(header);
                sb.Append("<ActivationResponse Version=\"1.0\"><Content>");
                foreach (var license in activationResponse.Content)
                {
                    sb.Append($"<License GUID=\"{license.GUID}\">");
                    sb.Append($"<![CDATA[{license.Value}]]>");
                    sb.Append($"</License>");
                }
                sb.Append("</Content></ActivationResponse>");
                File.WriteAllText(path, sb.ToString());
                Console.WriteLine($@"License 已写入 {path}");
            }
        }

        static void AdjustLicenses(List<ILLicense> licenses,string dllDirectory)
        {
            var maxVersion = new Version(9999, 0);
            var minVersion = new Version(0, 0);
            var begin = DateTime.Now;
            var end = DateTime.Now.AddDays(6000);
            foreach (var lic in licenses)
            {
                lic.From = begin;
                lic.To = end;
                lic.IsEvaluation = false;
                lic.MaxVersion = maxVersion;
                lic.MinVersion = minVersion;
                if (lic is ILDevelopmentLicense ilDevelopmentLicense)
                {
                    ilDevelopmentLicense.TargetFrom = begin;
                    ilDevelopmentLicense.TargetTo= end;
                }
                Console.WriteLine($"LicenseKey={lic.LicenseKey}{Environment.NewLine}");
            }
        }

        static List<ILLicense> LoadLicenses()
        {
            var licensesStringsFromRegistry = LicenseManager.GetLicensesStringsFromRegistry();
            var results = new List<ILLicense>();
            foreach (var item in licensesStringsFromRegistry)
            {
                var result = ILLicense.CreateWithoutVerify(item);
                if (result == null) continue;
                results.Add(result);
            }
            return results;
        }

        static int GetPositionAfterMatch(byte[] data, byte[] pattern)
        {
            for (int i = 0; i < (data.Length - pattern.Length); i++)
            {
                bool match = true;
                for (int k = 0; k < pattern.Length; k++)
                {
                    if (data[i + k] != pattern[k])
                    {
                        match = false;
                        break;
                    }
                }
                if (match)
                {
                    return i + pattern.Length;
                }
            }
            return -1;
        }
    }
}
