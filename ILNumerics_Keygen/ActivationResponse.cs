﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml.Serialization;

namespace ILNumerics_Keygen
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public  class ActivationResponse
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private License[] contentField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private decimal versionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("License", IsNullable = false)]
        public License[] Content
        {
            get
            {
                return this.contentField;
            }
            set
            {
                this.contentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public decimal Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public  class License
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string gUIDField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string GUID
        {
            get
            {
                return this.gUIDField;
            }
            set
            {
                this.gUIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }

        [XmlIgnore]
        public string LicenseString => Encoding.Unicode.GetString(Convert.FromBase64String(Value));
    }
}
