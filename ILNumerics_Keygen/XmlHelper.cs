﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ILNumerics_Keygen
{
    public static class XmlHelper
    {
        public static string ToXml<T>(this object obj, bool indent = false, string newLineChars = "")
        {
            XmlWriterSettings settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = indent,
                NewLineChars = newLineChars
            };

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            using var ms = new MemoryStream();
            XmlWriter writer = XmlWriter.Create(ms, settings);
            XmlSerializer xs = new XmlSerializer(typeof(T));
            xs.Serialize(writer, obj, ns);
            Encoding encoding = new UTF8Encoding(false);
            ms.Seek(0, SeekOrigin.Begin);
            using StreamReader reader = new StreamReader(ms, encoding);

            var result = reader.ReadToEnd();
            return result;
        }

        public static T FromXml<T>(this string xml) where T : new()
        {
            if (string.IsNullOrEmpty(xml)) return default;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            StringReader stringReader = new StringReader(xml);
            var xmlObject = (T)xmlSerializer.Deserialize(stringReader);
            return xmlObject;
        }

        private static string ConvertXmlToString(this XmlDocument xmlDoc)
        {
            MemoryStream stream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(stream, null);
            writer.Formatting = Formatting.Indented;
            xmlDoc.Save(writer);
            StreamReader sr = new StreamReader(stream, System.Text.Encoding.UTF8);
            stream.Position = 0;
            string xmlString = sr.ReadToEnd();
            sr.Close();
            stream.Close();
            return xmlString;
        }
    }
}
