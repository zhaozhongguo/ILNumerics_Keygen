﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Security;
using System.Threading;
using ILNumerics.Licensing;
using Microsoft.Win32;
using LicenseManager = System.ComponentModel.LicenseManager;

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming
// ReSharper disable CheckNamespace


namespace ILLicProvider
{
    // Token: 0x02000002 RID: 2
    [AttributeUsage(AttributeTargets.Property)]
    public class ParameterAttribute : Attribute
    {
        // Token: 0x17000001 RID: 1
        // (get) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
        public bool IsFlag
        {
            get
            {
                return flagType;
            }
        }

        // Token: 0x17000002 RID: 2
        // (get) Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000258
        public string LongName
        {
            get
            {
                return longName;
            }
        }

        // Token: 0x17000003 RID: 3
        // (get) Token: 0x06000003 RID: 3 RVA: 0x00002060 File Offset: 0x00000260
        public string HelpText
        {
            get
            {
                return helpText;
            }
        }

        // Token: 0x17000004 RID: 4
        // (get) Token: 0x06000004 RID: 4 RVA: 0x00002068 File Offset: 0x00000268
        public Type ParameterType
        {
            get
            {
                return type;
            }
        }

        // Token: 0x17000005 RID: 5
        // (get) Token: 0x06000005 RID: 5 RVA: 0x00002070 File Offset: 0x00000270
        public object DefaultValue
        {
            get
            {
                return defaultValue;
            }
        }

        // Token: 0x17000006 RID: 6
        // (get) Token: 0x06000006 RID: 6 RVA: 0x00002078 File Offset: 0x00000278
        public object Value
        {
            get
            {
                return value;
            }
        }

        // Token: 0x06000007 RID: 7 RVA: 0x00002080 File Offset: 0x00000280
        public ParameterAttribute(string longname, string help, Type paramType, object defValue)
        {
            longName = longname;
            helpText = help;
            type = paramType;
            defaultValue = defValue;
            value = defValue;
            flagType = false;
        }

        // Token: 0x06000008 RID: 8 RVA: 0x000020B4 File Offset: 0x000002B4
        public ParameterAttribute(string longname, string help)
        {
            longName = longname;
            helpText = help;
            flagType = true;
            defaultValue = false;
            value = false;
            type = typeof(bool);
        }

        // Token: 0x06000009 RID: 9 RVA: 0x00002104 File Offset: 0x00000304
        public void SetValue(object value)
        {
            this.value = value;
        }

        // Token: 0x0600000A RID: 10 RVA: 0x0000210D File Offset: 0x0000030D
        public object GetValue()
        {
            if (value == null)
            {
                return defaultValue;
            }
            return value;
        }

        // Token: 0x17000007 RID: 7
        // (get) Token: 0x0600000B RID: 11 RVA: 0x00002124 File Offset: 0x00000324
        public string Help
        {
            get
            {
                string str = string.Format("     -{0}{1}\n", longName, IsFlag ? "" : "=value");
                string text = defaultValue.ToString();
                str += string.Format("\tDefault value: {0}\n", string.IsNullOrEmpty(text) ? "Empty string" : text);
                if (flagType)
                {
                    str += "\tType: Boolean Flag\n";
                }
                else
                {
                    str += string.Format("\tType: {0}\n", type);
                }
                str = str + "\t" + helpText;
                return str + Environment.NewLine;
            }
        }

        // Token: 0x0600000C RID: 12 RVA: 0x000021D2 File Offset: 0x000003D2
        public override string ToString()
        {
            return LongName + "=" + value;
        }

        // Token: 0x04000001 RID: 1
        private string longName;

        // Token: 0x04000002 RID: 2
        private string helpText;

        // Token: 0x04000003 RID: 3
        private Type type;

        // Token: 0x04000004 RID: 4
        private object defaultValue;

        // Token: 0x04000005 RID: 5
        private object value;

        // Token: 0x04000006 RID: 6
        private bool flagType;
    }

    // Token: 0x02000003 RID: 3
    public static class ParameterHandler
    {
        // Token: 0x0600000D RID: 13 RVA: 0x000021F0 File Offset: 0x000003F0
        public static void Initialize(Type resolverClass)
        {
            parameterTable = new Hashtable();
            ResolveAttributes(resolverClass);
            foreach (ParameterAttribute parameterAttribute in parameters)
            {
                if (parameterAttribute.ParameterType == typeof(List<string>))
                {
                    parameterTable.Add(parameterAttribute.LongName, new List<string>());
                }
                else
                {
                    parameterTable.Add(parameterAttribute.LongName, Convert.ChangeType(parameterAttribute.DefaultValue, parameterAttribute.ParameterType));
                }
            }
            ParseArgs(Environment.GetCommandLineArgs());
        }

        // Token: 0x0600000E RID: 14 RVA: 0x000022A4 File Offset: 0x000004A4
        public static T GetValue<T>(string key)
        {
            object obj = null;
            if (!string.IsNullOrEmpty(key))
            {
                obj = parameterTable[key];
            }
            if (obj == null)
            {
                return default;
            }
            return (T)obj;
        }

        // Token: 0x0600000F RID: 15 RVA: 0x000022DC File Offset: 0x000004DC
        private static void ParseArgs(string[] args)
        {
            foreach (string arg in args)
            {
                foreach (ParameterAttribute param in parameters)
                {
                    if (IsPresent(arg, param))
                    {
                        ParseValue(arg, param);
                        break;
                    }
                }
            }
        }

        // Token: 0x06000010 RID: 16 RVA: 0x00002350 File Offset: 0x00000550
        private static void ParseValue(string arg, ParameterAttribute param)
        {
            object value = param.DefaultValue;
            if (!param.IsFlag)
            {
                string text = arg.Split(new[]
                {
                    "="
                }, 2, StringSplitOptions.RemoveEmptyEntries)[1];
                value = param.DefaultValue;
                if (param.ParameterType == typeof(List<string>))
                {
                    if (text.Contains(","))
                    {
                        value = new List<string>(text.Split(new[]
                        {
                            ','
                        }));
                    }
                    else
                    {
                        value = new List<string>(new[]
                        {
                            text
                        });
                    }
                }
                else
                {
                    value = text;
                }
            }
            else
            {
                value = true;
            }
            parameterTable[param.LongName] = Convert.ChangeType(value, param.ParameterType);
        }

        // Token: 0x06000011 RID: 17 RVA: 0x00002404 File Offset: 0x00000604
        private static bool IsPresent(string arg, ParameterAttribute param)
        {
            bool result = false;
            if (param.IsFlag)
            {
                if (arg.StartsWith("-" + param.LongName))
                {
                    result = true;
                }
            }
            else if (arg.StartsWith("-" + param.LongName + "="))
            {
                result = true;
            }
            return result;
        }

        // Token: 0x06000012 RID: 18 RVA: 0x00002458 File Offset: 0x00000658
        private static void ResolveAttributes(Type resolverClass)
        {
            properties = resolverClass.GetProperties(BindingFlags.Static | BindingFlags.Public);
            PropertyInfo[] array = properties;
            for (int i = 0; i < array.Length; i++)
            {
                object[] customAttributes = array[i].GetCustomAttributes(typeof(ParameterAttribute), true);
                if (customAttributes != null && customAttributes.Length == 1)
                {
                    parameters.Add((ParameterAttribute)customAttributes[0]);
                }
            }
        }

        // Token: 0x17000008 RID: 8
        // (get) Token: 0x06000013 RID: 19 RVA: 0x000024B8 File Offset: 0x000006B8
        public static string Help
        {
            get
            {
                string text = string.Empty;
                foreach (ParameterAttribute parameterAttribute in parameters)
                {
                    text = text + parameterAttribute.Help + Environment.NewLine;
                }
                return text;
            }
        }

        // Token: 0x04000007 RID: 7
        private static Hashtable parameterTable = new Hashtable();

        // Token: 0x04000008 RID: 8
        private static List<ParameterAttribute> parameters = new List<ParameterAttribute>();

        // Token: 0x04000009 RID: 9
        private static PropertyInfo[] properties = new PropertyInfo[0];
    }
    // Token: 0x02000006 RID: 6
    [SecuritySafeCritical]
    internal class ILLicProviderStartup
    {
        // Token: 0x02000004 RID: 4
        public static class Params
        {
            // Token: 0x17000009 RID: 9
            // (get) Token: 0x06000015 RID: 21 RVA: 0x0000253D File Offset: 0x0000073D
            [Parameter("email", "E-mail address.", typeof(string), "")]
            public static string Email
            {
                get
                {
                    return ParameterHandler.GetValue<string>("email");
                }
            }

            // Token: 0x1700000A RID: 10
            // (get) Token: 0x06000016 RID: 22 RVA: 0x00002549 File Offset: 0x00000749
            [Parameter("license", "License key.", typeof(string), "")]
            public static string LicenseKey
            {
                get
                {
                    return ParameterHandler.GetValue<string>("license");
                }
            }

            // Token: 0x1700000B RID: 11
            // (get) Token: 0x06000017 RID: 23 RVA: 0x00002555 File Offset: 0x00000755
            [Parameter("machine", "Unique machine key.", typeof(string), "")]
            public static string MachineKey
            {
                get
                {
                    return ParameterHandler.GetValue<string>("machine");
                }
            }

            // Token: 0x1700000C RID: 12
            // (get) Token: 0x06000018 RID: 24 RVA: 0x00002561 File Offset: 0x00000761
            [Parameter("install", "Flag to install licenses.")]
            public static bool Install
            {
                get
                {
                    return ParameterHandler.GetValue<bool>("install");
                }
            }

            // Token: 0x1700000D RID: 13
            // (get) Token: 0x06000019 RID: 25 RVA: 0x0000256D File Offset: 0x0000076D
            [Parameter("uninstall", "Flag to uninstall licenses.")]
            public static bool Uninstall
            {
                get
                {
                    return ParameterHandler.GetValue<bool>("uninstall");
                }
            }
        }
        public enum ReturnCodes
        {
            // Token: 0x0400000B RID: 11
            OK,
            // Token: 0x0400000C RID: 12
            HelpDisplayed,
            // Token: 0x0400000D RID: 13
            FailedToDownloadLicenses,
            // Token: 0x0400000E RID: 14
            FailedToSetLicensesInRegistry,
            // Token: 0x0400000F RID: 15
            FailedToRemoveLicensesFromRegistry,
            // Token: 0x04000010 RID: 16
            LicenseFileNotFound,
            // Token: 0x04000011 RID: 17
            LicenseFileReadingException,
            // Token: 0x04000012 RID: 18
            LicenseFileSavingException,
            // Token: 0x04000013 RID: 19
            LicenseFileIsEmpty
        }
        // Token: 0x0600001A RID: 26 RVA: 0x0000257C File Offset: 0x0000077C
        private static void Main(string[] args)
        {
            Console.WriteLine("ILLicProvider V{0} is started...", Assembly.GetExecutingAssembly().GetName().Version);
            ParameterHandler.Initialize(typeof(Params));
            if (args.Length == 0)
            {
                DisplayHelp();
                return;
            }
           DisplayInstallation();
            if (Params.Uninstall)
            {
                Console.WriteLine("Uninstalling licenses...");
                HandleRemovingCase();
                ExitWith(ReturnCodes.OK);
                return;
            }
            Console.WriteLine("Installing licenses...");
            if (string.IsNullOrEmpty(Params.LicenseKey) || string.IsNullOrEmpty(Params.Email))
            {
                DisplayHelp();
                return;
            }
            string text = string.IsNullOrEmpty(Params.MachineKey) ? ILNumerics.Licensing.LicenseManager.GetMachineHash() : Params.MachineKey;
            if (string.IsNullOrEmpty(Params.MachineKey))
            {
                Console.WriteLine("Machine key was not provided, local machine's key will be used.");
            }
            Console.WriteLine("MachineKey: {0}", text);
            if (!HandleFileCase(text))
            {
                Console.WriteLine("Failed to load licenses from the given license file!");
                HandleDownloadCase(text);
                return;
            }
            ExitWith(ReturnCodes.OK);
        }

        // Token: 0x0600001B RID: 27 RVA: 0x00002668 File Offset: 0x00000868
        public static void HandleDownloadCase(string machineKey)
        {
            Console.WriteLine("Attempt to download licenses.");
            Console.WriteLine("E-mail    : {0}", Params.Email);
            Console.WriteLine("Lic. key  : {0}", Params.LicenseKey);
            string text = DownloadFromServer(Params.Email, Params.LicenseKey, machineKey, Params.Install);
            if (string.IsNullOrEmpty(text))
            {
                Console.WriteLine("Empty license content was downloaded.");
                ExitWith(ReturnCodes.LicenseFileIsEmpty);
                return;
            }
            SaveLicenseFile(text, machineKey.Replace('/', '_') + ".lic");
            if (Params.Install)
            {
                SetMailAndKeyInRegistry(Params.Email, Params.LicenseKey);
                Console.WriteLine("Registry is set and licenses are installed.");
                ExitWith(ReturnCodes.OK);
                return;
            }
            Console.WriteLine("Licenses are not installed but downloaded and saved.");
            ExitWith(ReturnCodes.OK);
        }

        // Token: 0x0600001C RID: 28 RVA: 0x00002720 File Offset: 0x00000920
        public static bool HandleFileCase(string machineKey)
        {
            Console.WriteLine("Attempt to read license file.");
            string text = machineKey.Replace('/', '_') + ".lic";
            Console.WriteLine("Lic. file : {0}", text);
            bool result;
            if (File.Exists(text))
            {
                Console.WriteLine("License file is found.");
                string text2 = ReadLicenseFile(text);
                if (!string.IsNullOrEmpty(text2))
                {
                    if (Params.Install)
                    {
                        ILNumerics.Licensing.LicenseManager.SetLicenses(text2);
                        SetMailAndKeyInRegistry(Params.Email, Params.LicenseKey);
                        Console.WriteLine("Registry is set and licenses are installed.");
                    }
                    result = true;
                }
                else
                {
                    Console.WriteLine("Empty license file found.");
                    result = false;
                }
            }
            else
            {
                Console.WriteLine("License file is NOT found!");
                result = false;
            }
            return result;
        }

        // Token: 0x0600001D RID: 29 RVA: 0x000027C8 File Offset: 0x000009C8
        public static bool SetMailAndKeyInRegistry(string mail, string key)
        {
            bool result = false;
            using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\", true))
            {
                if (registryKey != null)
                {
                    registryKey.SetValue("LicenseKey", key);
                    registryKey.SetValue("EmailAddress", mail);
                    result = true;
                }
                else
                {
                    Console.WriteLine("Failed to edit registry - local machine!");
                    ExitWith(ReturnCodes.FailedToSetLicensesInRegistry);
                }
            }
            return result;
        }

        // Token: 0x0600001E RID: 30 RVA: 0x00002834 File Offset: 0x00000A34
        public static string ReadLicenseFile(string path)
        {
            string result = string.Empty;
            try
            {
                result = File.ReadAllText(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during reading license file:{0}{1}", Environment.NewLine, ex.Message);
                ExitWith(ReturnCodes.LicenseFileReadingException);
            }
            return result;
        }

        // Token: 0x0600001F RID: 31 RVA: 0x00002880 File Offset: 0x00000A80
        public static bool SaveLicenseFile(string content, string filename)
        {
            bool result = false;
            try
            {
                string text = Path.Combine(filename);
                File.WriteAllText(text, content);
                result = true;
                Console.WriteLine("License file is saved successfully: {0}", text);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during saving license file:{0}{1}", Environment.NewLine, ex.Message);
                ExitWith(ReturnCodes.LicenseFileSavingException);
            }
            return result;
        }

        // Token: 0x06000020 RID: 32 RVA: 0x000028E4 File Offset: 0x00000AE4
        public static bool IsILNumericsInstalled()
        {
            bool result = false;
            using (RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("Software\\ILNumerics\\ILNumerics Ultimate VS\\", false))
            {
                if (registryKey != null)
                {
                    string text = registryKey.GetValue("InstallDir") as string;
                    if (!string.IsNullOrEmpty(text))
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(text);
                        if (directoryInfo.Exists)
                        {
                            result = directoryInfo.Exists;
                        }
                        else
                        {
                            Console.WriteLine("ILNumerics path written in registry does not exist in the file system!");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Cannot find ILNumerics entry in registry.");
                }
            }
            return result;
        }

        // Token: 0x06000021 RID: 33 RVA: 0x00002978 File Offset: 0x00000B78
        public static void HandleRemovingCase()
        {
            using (RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry32))
            {
                try
                {
                    registryKey.DeleteSubKeyTree("Software\\ILNumerics");
                    Console.WriteLine("ILNumerics is already uninstalled from registry.");
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Cannot delete a subkey tree because the subkey does not exist."))
                    {
                        Console.WriteLine("ILNumerics is already uninstalled from registry.");
                    }
                    else
                    {
                        Console.WriteLine("Exception during uninstalling licenses from registry:{0}{1}", Environment.NewLine, ex.Message);
                        ExitWith(ReturnCodes.FailedToRemoveLicensesFromRegistry);
                    }
                }
            }
        }

        // Token: 0x06000022 RID: 34 RVA: 0x00002A10 File Offset: 0x00000C10
        private static string DownloadFromServer(string mail, string key, string machine, bool install)
        {
            string licenseFileContent = string.Empty;
            Action<LicensingSVCResponse> responseCallback = delegate (LicensingSVCResponse r)
            {
                if (!r.IsSuccess)
                {
                    Console.WriteLine("Failed to download licenses from ILNumerics Server!");
                    ExitWith(ReturnCodes.FailedToDownloadLicenses);
                    return;
                }
                HasDownloaded = true;
                licenseFileContent = r.Content;
                Console.WriteLine("Licenses downloaded!");
            };
            new LicSVCClient
            {
                DoInstall = install
            }.BeginDownloadLicenses(responseCallback, new Guid(key), mail, null, null, machine);
            int num = 50;
            int num2 = 10000 / num;
            while (!HasDownloaded)
            {
                Thread.Sleep(num);
                num2--;
                if (num2 == 0)
                {
                    ExitWith(ReturnCodes.FailedToDownloadLicenses);
                    Console.WriteLine("10 seconds timeout reached!" + Environment.NewLine + "Failed to download licenses from ILNumerics Server.");
                }
            }
            return licenseFileContent;
        }

        // Token: 0x06000023 RID: 35 RVA: 0x00002AA0 File Offset: 0x00000CA0
        public static void DisplayHelp()
        {
            Console.WriteLine("{0}There were no arguments specified or at least one was not correct.{1}Please read the official ILNumerics documentation and this help text!{2}", Environment.NewLine, Environment.NewLine, Environment.NewLine);
            Console.WriteLine("Available command line arguments:");
            Console.WriteLine(ParameterHandler.Help);
            Console.WriteLine("Please provide the E-mail and License and the Machine Key arguments to set up the licenses.");
            Console.WriteLine("If you would like to uninstall the licenses, use the Uninstall argument.");
            Console.WriteLine("{0}Press an Enter to exit...", Environment.NewLine);
            Console.ReadLine();
            ExitWith(ReturnCodes.HelpDisplayed);
        }

        // Token: 0x06000024 RID: 36 RVA: 0x00002B09 File Offset: 0x00000D09
        public static void DisplayInstallation()
        {
            if (!IsILNumericsInstalled())
            {
                Console.WriteLine("ILNumerics is NOT found to be installed.");
                return;
            }
            Console.WriteLine("ILNumerics is found to be installed.");
        }

        // Token: 0x06000025 RID: 37 RVA: 0x00002B27 File Offset: 0x00000D27
        public static void ExitWith(ReturnCodes rc)
        {
            Console.WriteLine("ReturnCode: {0} = {1}", (int)rc, rc.ToString());
            Environment.Exit((int)rc);
        }

        // Token: 0x04000014 RID: 20
        private const string DefaultInstallDirLocation = "Software\\ILNumerics\\ILNumerics Ultimate VS\\";

        // Token: 0x04000015 RID: 21
        private const string InstallDirKey = "InstallDir";

        // Token: 0x04000016 RID: 22
        private const string DefaultRegLicKeyRoot = "Software\\ILNumerics";

        // Token: 0x04000017 RID: 23
        private const string DefaultRegLicKeyLocationShared = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\";

        // Token: 0x04000018 RID: 24
        private const string UserLicenseKeyRegName = "LicenseKey";

        // Token: 0x04000019 RID: 25
        private const string UserEmailAddressRegName = "EmailAddress";

        // Token: 0x0400001A RID: 26
        private static bool HasDownloaded;
    }
}
