﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace ILNumerics.Licensing
{
    // Token: 0x02000033 RID: 51
    internal class DesigntimeContext : LicenseContext, ILicenseContext
    {
        // Token: 0x17000066 RID: 102
        // (get) Token: 0x060004F0 RID: 1264 RVA: 0x0001D4F2 File Offset: 0x0001B6F2
        public override LicenseUsageMode UsageMode
        {
            get
            {
                return LicenseUsageMode.Designtime;
            }
        }

        // Token: 0x17000067 RID: 103
        // (get) Token: 0x060004F1 RID: 1265 RVA: 0x0001D4F5 File Offset: 0x0001B6F5
        // (set) Token: 0x060004F2 RID: 1266 RVA: 0x0001D4FD File Offset: 0x0001B6FD
        public string RegistryRoot { get; private set; }

        // Token: 0x060004F3 RID: 1267 RVA: 0x0001D506 File Offset: 0x0001B706
        internal DesigntimeContext(string registryRoot)
        {
            RegistryRoot = registryRoot;
        }

        // Token: 0x060004F4 RID: 1268 RVA: 0x0001D520 File Offset: 0x0001B720
        public override void SetSavedLicenseKey(Type type, string key)
        {
            base.SetSavedLicenseKey(type, key);
            if (licenses.ContainsKey(type))
            {
                licenses[type] = key;
                return;
            }
            licenses.Add(type, key);
        }

        // Token: 0x060004F5 RID: 1269 RVA: 0x0001D553 File Offset: 0x0001B753
        public override string GetSavedLicenseKey(Type type, Assembly resourceAssembly)
        {
            return licenses[type];
        }

        // Token: 0x040000D0 RID: 208
        private Dictionary<Type, string> licenses = new Dictionary<Type, string>();
    }
}