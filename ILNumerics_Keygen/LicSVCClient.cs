﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;

namespace ILNumerics.Licensing
{
    // Token: 0x0200003A RID: 58
    internal class LicSVCClient
    {
        // Token: 0x0600054E RID: 1358 RVA: 0x0001F2BC File Offset: 0x0001D4BC
        internal void BeginDownloadLicenses(Action<LicensingSVCResponse> responseCallback, Guid guid, string email, Uri uri = null, string userAgent = null, string machineKey = null)
        {
            if (string.IsNullOrEmpty(userAgent))
            {
                userAgent = string.Format("ILNumerics_Ultimate_VS_LicSVC{0} Windows/{1} VS/??", typeof(LicSVCClient).Assembly.GetName().Version, Environment.OSVersion);
            }
            if (string.IsNullOrEmpty(machineKey))
            {
                machineKey = RuntimeLicenseProvider.GetMachineHash();
            }
            if (uri == null)
            {
                uri = new Uri(LicenseManager.LicenseDefaultWebSVCUri);
            }
            uri = new UriBuilder(uri)
            {
                Query = string.Format("guid={0}&email={1}&machinekey={2}&data={3}", guid.ToString(), Uri.EscapeDataString(email), Uri.EscapeDataString(machineKey), Uri.EscapeDataString(Environment.UserDomainName + "\\" + Environment.UserName))
            }.Uri;
            try
            {
                HttpWebRequest httpWebRequest = WebRequest.Create(uri) as HttpWebRequest;
                if (httpWebRequest != null)
                {
                    httpWebRequest.AllowAutoRedirect = true;
                    httpWebRequest.UserAgent = userAgent;
                    httpWebRequest.BeginGetResponse(OnResponse, Tuple.Create(httpWebRequest, responseCallback));
                }
            }
            catch (ConfigurationException)
            {
            }
            catch (NotSupportedException)
            {
            }
        }

        // Token: 0x0600054F RID: 1359 RVA: 0x0001F3F0 File Offset: 0x0001D5F0
        internal void OnResponse(IAsyncResult ar)
        {
            if (ar == null || !(ar.AsyncState is Tuple<HttpWebRequest, Action<LicensingSVCResponse>>))
            {
                return;
            }
            Tuple<HttpWebRequest, Action<LicensingSVCResponse>> tuple = ar.AsyncState as Tuple<HttpWebRequest, Action<LicensingSVCResponse>>;
            Action<LicensingSVCResponse> item = tuple.Item2;
            LicensingSVCResponse licensingSVCResponse = new LicensingSVCResponse();
            HttpWebRequest item2 = tuple.Item1;
            if (item2 == null || item2.ContentLength > 32767L)
            {
                return;
            }
            try
            {
                string text;
                using (Stream responseStream = item2.EndGetResponse(ar).GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    {
                        text = streamReader.ReadToEnd();
                    }
                }
                licensingSVCResponse.Content = text;
                if (string.IsNullOrWhiteSpace(text))
                {
                    licensingSVCResponse.Message = "No licenses found. Contact sales@ilnumerics.net in order to purchase ILNumerics products.";
                    licensingSVCResponse.IsSuccess = false;
                }
                else if (!string.IsNullOrEmpty(text))
                {
                    try
                    {
                        string message = "";
                        if (isError(text, ref message))
                        {
                            licensingSVCResponse.Message = message;
                            licensingSVCResponse.IsSuccess = false;
                        }
                        else
                        {
                            if (DoInstall)
                            {
                                RuntimeLicenseProvider.SetLicenses(text);
                            }
                            licensingSVCResponse.Message = string.Empty;
                            licensingSVCResponse.IsSuccess = true;
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        licensingSVCResponse.Message = "Failure installing licenses. Please try again! If the problem remains contact: support@ilnumerics.net";
                        licensingSVCResponse.IsSuccess = false;
                    }
                    catch (ArgumentException)
                    {
                        licensingSVCResponse.Message = "Failure installing licenses. Please try again! If the problem remains contact: support@ilnumerics.net";
                        licensingSVCResponse.IsSuccess = false;
                    }
                }
            }
            catch (WebException ex)
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry("ILNumerics Licensing SVC - Failure: " + ex, EventLogEntryType.Error, 101, 1);
                }
                if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response is HttpWebResponse && (ex.Response as HttpWebResponse).StatusCode == HttpStatusCode.NotFound)
                {
                    licensingSVCResponse.Message = "The ILNumerics licensing server might be temporarily unavailable. Please try again later! If the problem remains consider using offline activation or ask us for help: support@ilnumerics.net";
                }
                else
                {
                    licensingSVCResponse.Message = "There was an error communicating with the ILNumerics licensing server. Make sure your firewall is configured to allow outgoing connections from this computer. If the problem remains consider using offline activation or ask us for help: support@ilnumerics.net";
                }
            }
            catch (Exception ex2)
            {
                using (EventLog eventLog2 = new EventLog("Application"))
                {
                    eventLog2.Source = "Application";
                    eventLog2.WriteEntry("ILNumerics Licensing SVC - Failure: " + ex2, EventLogEntryType.Error, 101, 1);
                }
                licensingSVCResponse.Message = "There was an error downloading the licenses from the ILNumerics licensing server. If the problem remains consider using offline activation or ask us for help: support@ilnumerics.net";
            }
            if (item != null)
            {
                item(licensingSVCResponse);
            }
        }

        // Token: 0x06000550 RID: 1360 RVA: 0x0001F66C File Offset: 0x0001D86C
        private bool isError(string content, ref string msg)
        {
            if (!content.Trim().StartsWith("<?xml"))
            {
                msg = "A valid set of licenses could not get retrieved. Please try again! Please contact support@ilnumerics.net in case that this error remains.";
                return true;
            }
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(content);
            string[] array = (from XmlNode n in xmlDocument.SelectNodes("//ActivationResponse/Error")
                              select n.InnerText).ToArray();
            if (array != null && array.Length != 0)
            {
                msg = string.Join(Environment.NewLine, array);
                return true;
            }
            return false;
        }

        /// <summary>
        ///       By default: install downloaded licenses into the reg. Set this to false in order to not touch the lokal registry. 
        ///       </summary>
        // Token: 0x040000FE RID: 254
        internal bool DoInstall = true;
    }
}
