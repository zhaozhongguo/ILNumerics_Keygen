﻿namespace ILNumerics.Licensing
{
    /// <summary>
    ///       ILNumerics Licensing service's response.
    ///       <para>[ILNumerics Core Module]</para>
    /// </summary>
    /// <remarks>
    ///   <para>[ILNumerics Core Module]</para>
    /// </remarks>
    // Token: 0x02000039 RID: 57
    public class LicensingSVCResponse
    {
        /// <summary>
        ///       Message delivered from the licensing SVC. May be null.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700007A RID: 122
        // (get) Token: 0x06000547 RID: 1351 RVA: 0x0001F287 File Offset: 0x0001D487
        // (set) Token: 0x06000548 RID: 1352 RVA: 0x0001F28F File Offset: 0x0001D48F
        public string Message { get; set; }

        /// <summary>
        ///       Flag to indicate if the response was successeful.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700007B RID: 123
        // (get) Token: 0x06000549 RID: 1353 RVA: 0x0001F298 File Offset: 0x0001D498
        // (set) Token: 0x0600054A RID: 1354 RVA: 0x0001F2A0 File Offset: 0x0001D4A0
        public bool IsSuccess { get; set; }

        /// <summary>
        ///       Licenses fetched from the licensing server.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700007C RID: 124
        // (get) Token: 0x0600054B RID: 1355 RVA: 0x0001F2A9 File Offset: 0x0001D4A9
        // (set) Token: 0x0600054C RID: 1356 RVA: 0x0001F2B1 File Offset: 0x0001D4B1
        public string Content { get; set; }
    }
}