﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ILNumerics.Licensing
{
    using  Size=System.Drawing.Size;
    // Token: 0x02000002 RID: 2
    public class LicensesControl : UserControl
    {
        // Token: 0x14000001 RID: 1
        // (add) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
        // (remove) Token: 0x06000002 RID: 2 RVA: 0x00002088 File Offset: 0x00000288
        public event EventHandler Changed;

        // Token: 0x06000003 RID: 3 RVA: 0x000020BD File Offset: 0x000002BD
        protected void OnChanged()
        {
            if (this.Changed != null)
            {
                this.Changed(this, EventArgs.Empty);
            }
        }

        // Token: 0x17000001 RID: 1
        // (get) Token: 0x06000004 RID: 4 RVA: 0x000020D8 File Offset: 0x000002D8
        public bool IsDirty
        {
            get
            {
                return this.checkIsDirty();
            }
        }

        // Token: 0x06000005 RID: 5 RVA: 0x000020E0 File Offset: 0x000002E0
        public LicensesControl()
        {
            this.InitializeComponent();
        }

        // Token: 0x06000006 RID: 6 RVA: 0x000020F9 File Offset: 0x000002F9
        private void ILLicensesControl_Load(object sender, EventArgs e)
        {
            this.licensesGridView.DataSource = this.m_bindingList;
            this.fillLicensesTable();
            this.txtMachineKey.Text = RuntimeLicenseProvider.GetMachineHash();
            this.Reset();
        }

        // Token: 0x06000007 RID: 7 RVA: 0x00002128 File Offset: 0x00000328
        public void Reset()
        {
            Guid? userLicenseKey = this.GetUserLicenseKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\");
            if (userLicenseKey == null)
            {
                this.txtLicenseKey.ReadOnly = false;
                this.txtLicenseKey.Text = "(enter license key)";
            }
            else
            {
                this.txtLicenseKey.ReadOnly = false;
                this.txtLicenseKey.Text = userLicenseKey.GetValueOrDefault().ToString();
            }
            string userEmail = this.GetUserEmail("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\");
            if (string.IsNullOrEmpty(userEmail))
            {
                this.txtEmailAddress.ReadOnly = false;
                this.txtEmailAddress.Text = "(enter your email)";
                return;
            }
            this.txtEmailAddress.ReadOnly = false;
            this.txtEmailAddress.Text = userEmail;
        }

        // Token: 0x06000008 RID: 8 RVA: 0x000021E0 File Offset: 0x000003E0
        private bool checkIsDirty()
        {
            Guid? userLicenseKey = this.GetUserLicenseKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\");
            bool flag;
            if (userLicenseKey != null)
            {
                flag = (this.txtLicenseKey.Text.Trim().ToUpper() != userLicenseKey.GetValueOrDefault().ToString().ToUpper());
            }
            else
            {
                flag = (this.txtLicenseKey.Text != "(enter license key)");
            }
            string userEmail = this.GetUserEmail("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\");
            if (!string.IsNullOrEmpty(userEmail))
            {
                return flag | this.txtEmailAddress.Text.Trim().ToUpper() != userEmail.ToUpper();
            }
            return flag | this.txtEmailAddress.Text.Trim() != "(enter your email)";
        }

        // Token: 0x06000009 RID: 9 RVA: 0x000022A8 File Offset: 0x000004A8
        private void fillLicensesTable()
        {
            if (this.licensesGridView.InvokeRequired)
            {
                this.licensesGridView.Invoke(new Action(delegate ()
                {
                    this.fillLicensesTable();
                }));
                return;
            }
            List<LicenseViewModel> list = this.GetLicenses().ToList<LicenseViewModel>();
            this.licensesGridView.DataSource = new BindingList<LicenseViewModel>(list);
            this.licensesGridView.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            this.licensesGridView.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.licensesGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.licensesGridView.Columns.GetFirstColumn(DataGridViewElementStates.Displayed).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            this.licensesGridView.DefaultCellStyle.Padding = new Padding(3);
            this.licensesGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.licensesGridView.Columns.Clear();
            this.licensesGridView.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "ModuleColumn",
                HeaderText = "Module Name",
                DataPropertyName = "Module",
                ReadOnly = true
            });
            this.licensesGridView.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "DevelopmentColumn",
                HeaderText = "Development",
                DataPropertyName = "Development",
                ReadOnly = true
            });
            this.licensesGridView.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "ExecutionColumn",
                HeaderText = "Execution",
                DataPropertyName = "Execution",
                ReadOnly = true
            });
            this.licensesGridView.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "DistributionColumn",
                HeaderText = "Distribution",
                DataPropertyName = "Distribution",
                ReadOnly = true
            });
            this.licensesGridView.AutoGenerateColumns = false;
            this.licensesGridView.Refresh();
            int days = (list.Min((LicenseViewModel l) => l.To) - DateTime.Now).Days;
            if (days < 60)
            {
                this.labTrialInfo.Visible = true;
                this.labTrialInfo.Text = string.Format("Some licenses end/need refresh in {0} days!", days);
                return;
            }
            this.labTrialInfo.Visible = false;
        }

        // Token: 0x0600000A RID: 10 RVA: 0x000024F0 File Offset: 0x000006F0
        private IEnumerable<LicenseViewModel> GetLicenses()
        {
            IEnumerable<ILLicense> licenses = RuntimeLicenseProvider.GetLicenses();
            return RuntimeLicenseProvider.LicensedModulesFullNames.GroupJoin(licenses, (string l) => l, (ILLicense ik) => ik.TargetTypeName, delegate (string typeName, IEnumerable<ILLicense> list)
            {
                IEnumerable<ILLicense> enumerable = from glic in list
                                                    where glic.GetType() == typeof(ILRegRuntimeLicense)
                                                    select glic;
                IEnumerable<ILLicense> enumerable2 = from glic in list
                                                     where glic.GetType() == typeof(ILDevelopmentLicense)
                                                     select glic;
                string execution;
                if (enumerable2.Count<ILLicense>() == 1)
                {
                    ILDevelopmentLicense ildevelopmentLicense = enumerable2.First<ILLicense>() as ILDevelopmentLicense;
                    ILRegRuntimeLicense ilregRuntimeLicense2;
                    if (enumerable.Count<ILLicense>() != 1)
                    {
                        ILRegRuntimeLicense ilregRuntimeLicense = new ILRegRuntimeLicense();
                        ilregRuntimeLicense.From = ildevelopmentLicense.TargetFrom;
                        ilregRuntimeLicense2 = ilregRuntimeLicense;
                        ilregRuntimeLicense.To = ildevelopmentLicense.TargetTo;
                    }
                    else
                    {
                        ilregRuntimeLicense2 = (enumerable.First<ILLicense>() as ILRegRuntimeLicense);
                    }
                    ILRegRuntimeLicense ilregRuntimeLicense3 = ilregRuntimeLicense2;
                    if (ildevelopmentLicense.TargetTo > ilregRuntimeLicense3.To)
                    {
                        ilregRuntimeLicense3.To = ildevelopmentLicense.TargetTo;
                    }
                    execution = this.rangeFromLic(new ILRegRuntimeLicense[]
                    {
                        ilregRuntimeLicense3
                    }, false);
                }
                else
                {
                    execution = this.rangeFromLic(enumerable, false);
                }
                LicenseViewModel licenseViewModel = new LicenseViewModel();
                licenseViewModel.Module = this.typeName2ModuleName(typeName, list);
                licenseViewModel.Development = this.rangeFromLic(from glic in list
                                                                 where glic.GetType() == typeof(ILDevelopmentLicense)
                                                                 select glic, false);
                licenseViewModel.Execution = execution;
                licenseViewModel.Distribution = this.rangeFromLic(enumerable2, true);
                DateTime to;
                if (list == null || list.Count<ILLicense>() <= 0)
                {
                    to = DateTime.MaxValue;
                }
                else
                {
                    to = list.Min((ILLicense l) => l.To);
                }
                licenseViewModel.To = to;
                return licenseViewModel;
            });
        }

        // Token: 0x0600000B RID: 11 RVA: 0x00002558 File Offset: 0x00000758
        private string rangeFromLic(IEnumerable<ILLicense> list, bool useDistribValues = false)
        {
            if (list.Count<ILLicense>() == 0)
            {
                return "not available";
            }
            if (useDistribValues)
            {
                ILDevelopmentLicense ildevelopmentLicense = list.First<ILLicense>() as ILDevelopmentLicense;
                if (ildevelopmentLicense == null)
                {
                    return "not available";
                }
                string text = this.checkDate(ildevelopmentLicense.TargetFrom);
                string text2 = this.checkDate(ildevelopmentLicense.TargetTo);
                if (text == text2)
                {
                    return "not available";
                }
                return string.Concat(new string[]
                {
                    "From:",
                    text,
                    Environment.NewLine,
                    "To:   ",
                    text2
                });
            }
            else
            {
                ILRegRuntimeLicense ilregRuntimeLicense = list.First<ILLicense>() as ILRegRuntimeLicense;
                if (ilregRuntimeLicense == null)
                {
                    return "not available";
                }
                string text3 = this.checkDate(ilregRuntimeLicense.From);
                string text4 = this.checkDate(ilregRuntimeLicense.To);
                if (text3 == text4)
                {
                    return "not available";
                }
                return string.Concat(new string[]
                {
                    "From:",
                    text3,
                    Environment.NewLine,
                    "To:   ",
                    text4
                });
            }
        }

        // Token: 0x0600000C RID: 12 RVA: 0x00002650 File Offset: 0x00000850
        private string checkDate(DateTime date)
        {
            if (date == DateTime.MinValue || date == new DateTime(1975, 6, 10))
            {
                return "-inf";
            }
            if (date == DateTime.MaxValue || date == new DateTime(9999, 12, 31))
            {
                return "inf";
            }
            return date.ToShortDateString();
        }

        // Token: 0x0600000D RID: 13 RVA: 0x000026B8 File Offset: 0x000008B8
        private string typeName2ModuleName(string typeName, IEnumerable<ILLicense> lics)
        {
            string text = RuntimeLicenseProvider.GetModuleNameFromType(typeName);
            if (lics.Any((ILLicense l) => l.IsEvaluation))
            {
                text = text + Environment.NewLine + "-- Trial --";
            }
            return text;
        }

        // Token: 0x0600000E RID: 14 RVA: 0x00002708 File Offset: 0x00000908
        public Guid? GetUserLicenseKey(string regRoot = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\")
        {
            try
            {
                using (RegistryKey registryKey = Registry.CurrentUser.CreateSubKey(regRoot, RegistryKeyPermissionCheck.ReadSubTree))
                {
                    if (registryKey == null)
                    {
                        return null;
                    }
                    object value = registryKey.GetValue("LicenseKey");
                    if (value == null)
                    {
                        return null;
                    }
                    Guid value2;
                    if (Guid.TryParse(value.ToString(), out value2))
                    {
                        return new Guid?(value2);
                    }
                }
            }
            catch (SecurityException ex)
            {
                Trace.WriteLine("License Management: Error retrieving users license key. Read details below.");
                Trace.WriteLine(ex.ToString());
            }
            catch (IOException ex2)
            {
                Trace.WriteLine("License Management: Error retrieving users license key. Read details below.");
                Trace.WriteLine(ex2.ToString());
            }
            return null;
        }

        // Token: 0x0600000F RID: 15 RVA: 0x000027D4 File Offset: 0x000009D4
        public string GetUserEmail(string regRoot = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\")
        {
            try
            {
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(regRoot, RegistryKeyPermissionCheck.ReadSubTree))
                {
                    if (registryKey == null)
                    {
                        return null;
                    }
                    object value = registryKey.GetValue("EmailAddress");
                    if (value == null)
                    {
                        return null;
                    }
                    return value.ToString();
                }
            }
            catch (SecurityException ex)
            {
                Trace.WriteLine("License Management: Error retrieving users email address. Read details below.");
                Trace.WriteLine(ex.ToString());
            }
            catch (IOException ex2)
            {
                Trace.WriteLine("License Management: Error retrieving users email address. Read details below.");
                Trace.WriteLine(ex2.ToString());
            }
            return null;
        }

        // Token: 0x06000010 RID: 16 RVA: 0x00002874 File Offset: 0x00000A74
        private void btnImport_Click(object sender, EventArgs e)
        {
            if (this.GetUserLicenseKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\") == null)
            {
                string text = this.txtEmailAddress.Text.Trim();
                string input = this.txtLicenseKey.Text.Trim();
                if (string.IsNullOrEmpty(text) || text == "(enter your email)")
                {
                    MessageBox.Show("Please enter your email address in the text field!");
                    return;
                }
                Guid guid;
                if (!Guid.TryParse(input, out guid))
                {
                    MessageBox.Show("Please enter your license key in the text field!");
                    return;
                }
                this.storeRegData(guid.ToString(), "LicenseKey");
                this.storeRegData(text, "EmailAddress");
            }
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Offline activation: locate the file with your licenses...";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    RuntimeLicenseProvider.SetLicenses(File.ReadAllText(openFileDialog.FileName), "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\Licenses\\");
                    this.OnChanged();
                    MessageBox.Show("Congratulations! Your licenses have been updated.");
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Error reading file: " + ex.Message, "Licensing error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                catch (IOException ex2)
                {
                    MessageBox.Show("Error reading file: " + ex2.Message, "Licensing error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                catch (FormatException)
                {
                    MessageBox.Show("Error importing licenses. Either the file is not a valid licenses file or it has been corrupted. Please try again! If the problem persists acquire a new licenses file or contact support@ilnumerics.net!", "Import ILNumerics Licenses from File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        // Token: 0x06000011 RID: 17 RVA: 0x000029DC File Offset: 0x00000BDC
        public void RefreshLicenses(string email, string guid)
        {
            Guid checkedGuid = Guid.Empty;
            try
            {
                checkedGuid = Guid.Parse(guid);
            }
            catch (FormatException)
            {
                MessageBox.Show("The license key entered does not appear to be valid. Please enter the license key provided to you by ILNumerics with your purchase or with the trial download.", "ILNumerics Visual Studio Extension", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            Action<LicensingSVCResponse> responseCallback = delegate (LicensingSVCResponse r)
            {
                this.SetWorkingState(false);
                if (!r.IsSuccess)
                {
                    MessageBox.Show(r.Message, "ILNumerics License Activation Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                this.fillLicensesTable();
                this.storeRegData(checkedGuid.ToString(), "LicenseKey");
                this.storeRegData(email, "EmailAddress");
                this.OnChanged();
                if (RuntimeLicenseProvider.GetLicenses().Count<ILLicense>() > 0)
                {
                    MessageBox.Show("Congratulations! Your licenses are now up to date.", "ILNumerics License Management", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
                Version version = typeof(ILLicense).Assembly.GetName().Version;
                MessageBox.Show("Your licenses have been updated. No licenses are currently active. \r\n                            \r\nCheck the timespans and versions of your licenses and make sure that the licenses were obtained for the current date and for the version of ILNumerics installed! ILNumerics version installed is: " + version + "\r\nVisit your account for a list of all your current licenses: https://ilnumerics.net/mylicenses.php", "ILNumerics License Management", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            };
            this.SetWorkingState(true);
            RuntimeLicenseProvider.BeginRefreshLicenses(responseCallback, checkedGuid, email, null, null);
        }

        // Token: 0x06000012 RID: 18 RVA: 0x00002A64 File Offset: 0x00000C64
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            string email = this.txtEmailAddress.Text.Trim();
            string guid = this.txtLicenseKey.Text.Trim();
            this.RefreshLicenses(email, guid);
        }

        // Token: 0x06000013 RID: 19 RVA: 0x00002A9C File Offset: 0x00000C9C
        private void storeRegData(string p, string regName)
        {
            try
            {
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\", RegistryKeyPermissionCheck.ReadWriteSubTree))
                {
                    object value = registryKey.GetValue(regName);
                    if (value == null || (value is string && value.ToString() != p))
                    {
                        registryKey.SetValue(regName, p);
                    }
                }
            }
            catch (SecurityException ex)
            {
                this.LogError("License Management: Error storing users license data: '{0}'. Read details below.", new object[]
                {
                    regName
                });
                this.LogError(ex.ToString(), new object[0]);
            }
            catch (IOException ex2)
            {
                this.LogError("License Management: Error storing users license data: '{0}'. Read details below.", new object[]
                {
                    regName
                });
                this.LogError(ex2.ToString(), new object[0]);
            }
        }

        // Token: 0x06000014 RID: 20 RVA: 0x00002B70 File Offset: 0x00000D70
        private void SetWorkingState(bool working)
        {
            if (this.btnRefresh.InvokeRequired)
            {
                Action method = delegate ()
                {
                    this.SetWorkingState(working);
                };
                this.btnRefresh.Invoke(method, null);
                return;
            }
            if (working)
            {
                //this.btnRefresh.Image = Resource1.ajax_loader;
                this.btnRefresh.Text = "";
                this.btnRefresh.Refresh();
                return;
            }
            this.btnRefresh.Image = null;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Refresh();
        }

        // Token: 0x06000015 RID: 21 RVA: 0x00002C18 File Offset: 0x00000E18
        private void LogError(string msg, params object[] parameters)
        {
            try
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry(string.Format(msg, parameters), EventLogEntryType.Error, 101, 1);
                }
            }
            catch
            {
            }
        }

        // Token: 0x06000016 RID: 22 RVA: 0x00002C7C File Offset: 0x00000E7C
        private void txtBox_Enter(object sender, EventArgs e)
        {
            TextBox txtbox = sender as TextBox;
            if (txtbox != null && !txtbox.ReadOnly && txtbox.Enabled)
            {
                base.BeginInvoke(new Action(delegate ()
                {
                    txtbox.SelectAll();
                    txtbox.Focus();
                }));
            }
        }

        // Token: 0x06000017 RID: 23 RVA: 0x00002CD0 File Offset: 0x00000ED0
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        // Token: 0x06000018 RID: 24 RVA: 0x00002CF0 File Offset: 0x00000EF0
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
            this.tableLayoutPanel1 = new TableLayoutPanel();
            this.groupBox1 = new GroupBox();
            this.labTrialInfo = new Label();
            this.label5 = new Label();
            this.txtMachineKey = new TextBox();
            this.txtEmailAddress = new TextBox();
            this.txtLicenseKey = new TextBox();
            this.label4 = new Label();
            this.label3 = new Label();
            this.btnImport = new Button();
            this.btnRefresh = new Button();
            this.label2 = new Label();
            this.linkLabel2 = new LinkLabel();
            this.linkLabel1 = new LinkLabel();
            this.groupBox2 = new GroupBox();
            this.licensesGridView = new DataGridView();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((ISupportInitialize)this.licensesGridView).BeginInit();
            base.SuspendLayout();
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 0f));
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 0f));
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 1);
            this.tableLayoutPanel1.Dock = DockStyle.Fill;
            this.tableLayoutPanel1.Location = new Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 0f));
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 173f));
            this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 0f));
            this.tableLayoutPanel1.Size = new Size(404, 375);
            this.tableLayoutPanel1.TabIndex = 0;
            this.groupBox1.Controls.Add(this.labTrialInfo);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtMachineKey);
            this.groupBox1.Controls.Add(this.txtEmailAddress);
            this.groupBox1.Controls.Add(this.txtLicenseKey);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btnImport);
            this.groupBox1.Controls.Add(this.btnRefresh);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.linkLabel2);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Dock = DockStyle.Fill;
            this.groupBox1.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.groupBox1.Location = new Point(3, 205);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new Size(398, 167);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Update Licenses";
            this.labTrialInfo.AutoSize = true;
            this.labTrialInfo.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Italic, GraphicsUnit.Point, 0);
            this.labTrialInfo.ForeColor = Color.FromArgb(192, 0, 0);
            this.labTrialInfo.Location = new Point(9, 83);
            this.labTrialInfo.Name = "labTrialInfo";
            this.labTrialInfo.Size = new Size(165, 13);
            this.labTrialInfo.TabIndex = 0;
            this.labTrialInfo.Text = "You have days left for evaluation.";
            this.labTrialInfo.Visible = false;
            this.label5.AutoSize = true;
            this.label5.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.label5.Location = new Point(5, 116);
            this.label5.Name = "label5";
            this.label5.Size = new Size(72, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Machine Key:";
            this.txtMachineKey.BorderStyle = BorderStyle.None;
            this.txtMachineKey.Font = new Font("Consolas", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.txtMachineKey.Location = new Point(86, 115);
            this.txtMachineKey.Name = "txtMachineKey";
            this.txtMachineKey.ReadOnly = true;
            this.txtMachineKey.Size = new Size(206, 16);
            this.txtMachineKey.TabIndex = 4;
            this.txtEmailAddress.BorderStyle = BorderStyle.FixedSingle;
            this.txtEmailAddress.Font = new Font("Consolas", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.txtEmailAddress.Location = new Point(150, 16);
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.txtEmailAddress.ReadOnly = true;
            this.txtEmailAddress.Size = new Size(239, 23);
            this.txtEmailAddress.TabIndex = 2;
            this.txtEmailAddress.Enter += this.txtBox_Enter;
            this.txtLicenseKey.BorderStyle = BorderStyle.FixedSingle;
            this.txtLicenseKey.Font = new Font("Consolas", 9.75f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.txtLicenseKey.Location = new Point(150, 46);
            this.txtLicenseKey.Name = "txtLicenseKey";
            this.txtLicenseKey.ReadOnly = true;
            this.txtLicenseKey.Size = new Size(239, 23);
            this.txtLicenseKey.TabIndex = 3;
            this.txtLicenseKey.Enter += this.txtBox_Enter;
            this.label4.AutoSize = true;
            this.label4.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.label4.Location = new Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new Size(76, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Email Address:";
            this.label3.AutoSize = true;
            this.label3.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.label3.Location = new Point(6, 53);
            this.label3.Name = "label3";
            this.label3.Size = new Size(68, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "License Key:";
            this.btnImport.BackColor = SystemColors.ButtonFace;
            this.btnImport.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.btnImport.ForeColor = SystemColors.ControlText;
            this.btnImport.Location = new Point(306, 109);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new Size(83, 27);
            this.btnImport.TabIndex = 6;
            this.btnImport.Text = "Import";
            this.btnImport.UseVisualStyleBackColor = false;
            this.btnImport.Click += this.btnImport_Click;
            this.btnRefresh.Font = new Font("Microsoft Sans Serif", 9f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.btnRefresh.Location = new Point(306, 76);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new Size(83, 27);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "Activate";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += this.btnRefresh_Click;
            this.label2.AutoSize = true;
            this.label2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular, GraphicsUnit.Point, 0);
            this.label2.Location = new Point(6, 146);
            this.label2.Name = "label2";
            this.label2.Size = new Size(100, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Purchase Licenses:";
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new Point(267, 146);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new Size(129, 13);
            this.linkLabel2.TabIndex = 8;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "https://ilnumerics.net";
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new Point(131, 146);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new Size(126, 13);
            this.linkLabel1.TabIndex = 7;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "sales@ilnumerics.net";
            this.groupBox2.Controls.Add(this.licensesGridView);
            this.groupBox2.Dock = DockStyle.Fill;
            this.groupBox2.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold, GraphicsUnit.Point, 0);
            this.groupBox2.Location = new Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new Size(398, 196);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Licensed ILNumerics Modules";
            this.licensesGridView.AllowUserToAddRows = false;
            this.licensesGridView.AllowUserToDeleteRows = false;
            this.licensesGridView.BackgroundColor = SystemColors.ControlLightLight;
            this.licensesGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.licensesGridView.Dock = DockStyle.Fill;
            this.licensesGridView.Location = new Point(3, 16);
            this.licensesGridView.Name = "licensesGridView";
            this.licensesGridView.RowHeadersVisible = false;
            dataGridViewCellStyle.Font = new Font("Microsoft Sans Serif", 8.25f);
            this.licensesGridView.RowsDefaultCellStyle = dataGridViewCellStyle;
            this.licensesGridView.Size = new Size(392, 177);
            this.licensesGridView.StandardTab = true;
            this.licensesGridView.TabIndex = 1;
            this.licensesGridView.TabStop = false;
            base.AutoScaleDimensions = new SizeF(6f, 13f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.Controls.Add(this.tableLayoutPanel1);
            base.Name = "LicensesControl";
            base.Size = new Size(404, 375);
            base.Load += this.ILLicensesControl_Load;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((ISupportInitialize)this.licensesGridView).EndInit();
            base.ResumeLayout(false);
        }

        // Token: 0x04000002 RID: 2
        private const string DefaultRegLicKeyLocationShared = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\";

        // Token: 0x04000003 RID: 3
        private const string UserLicenseKeyRegName = "LicenseKey";

        // Token: 0x04000004 RID: 4
        private const string UserEmailAddressRegName = "EmailAddress";

        // Token: 0x04000005 RID: 5
        private BindingList<LicenseViewModel> m_bindingList = new BindingList<LicenseViewModel>();

        // Token: 0x04000006 RID: 6
        private const string DefaultEmailText = "(enter your email)";

        // Token: 0x04000007 RID: 7
        private const string NOT_AVAILABLE = "not available";

        // Token: 0x04000008 RID: 8
        private IContainer components;

        // Token: 0x04000009 RID: 9
        private TableLayoutPanel tableLayoutPanel1;

        // Token: 0x0400000A RID: 10
        private GroupBox groupBox1;

        // Token: 0x0400000B RID: 11
        private GroupBox groupBox2;

        // Token: 0x0400000C RID: 12
        private DataGridView licensesGridView;

        // Token: 0x0400000D RID: 13
        private Button btnRefresh;

        // Token: 0x0400000E RID: 14
        private Label label2;

        // Token: 0x0400000F RID: 15
        private LinkLabel linkLabel2;

        // Token: 0x04000010 RID: 16
        private LinkLabel linkLabel1;

        // Token: 0x04000011 RID: 17
        private TextBox txtLicenseKey;

        // Token: 0x04000012 RID: 18
        private Label label4;

        // Token: 0x04000013 RID: 19
        private Label label3;

        // Token: 0x04000014 RID: 20
        private Button btnImport;

        // Token: 0x04000015 RID: 21
        private Label label5;

        // Token: 0x04000016 RID: 22
        private TextBox txtEmailAddress;

        // Token: 0x04000017 RID: 23
        private TextBox txtMachineKey;

        // Token: 0x04000018 RID: 24
        private Label labTrialInfo;
    }
}
