﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ILNumerics.Licensing;
using LicenseManager = System.ComponentModel.LicenseManager;

namespace ILNumerics_Keygen
{
    public partial class LicenseInfoForm : Form
    {
        public LicenseInfoForm()
        {
            InitializeComponent();
            Load += LicenseInfoForm_Load;
        }

        private void LicenseInfoForm_Load(object sender, EventArgs e)
        {
            var control = new LicensesControl();
            control.Dock = DockStyle.Fill;
            this.Controls.Add(control);
        }
    }
}
