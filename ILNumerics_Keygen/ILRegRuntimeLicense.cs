﻿using System;
using System.Security;
using System.Xml;

namespace ILNumerics.Licensing
{
    /// <summary>
    ///       Allows the execution of types. Installed into / Loaded  from Registry. Does not restrict execution to individual assemblies. Tagged to an individual machine.
    ///       <para>[ILNumerics Core Module]</para>
    /// </summary>
    /// <remarks>
    ///   <para>[ILNumerics Core Module]</para>
    /// </remarks>
    // Token: 0x0200003B RID: 59
    [SecuritySafeCritical]
    [Serializable]
    public class ILRegRuntimeLicense : ILLicense
    {
        /// <summary>
        ///       base64 md5 hash of Netbios Name, OSVersion, ProcessorCount
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700007D RID: 125
        // (get) Token: 0x06000552 RID: 1362 RVA: 0x0001F6FF File Offset: 0x0001D8FF
        // (set) Token: 0x06000553 RID: 1363 RVA: 0x0001F707 File Offset: 0x0001D907
        internal string MachineKey { get; set; }

        /// <summary>
        ///       Create empty registry license (used for serialization)
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x06000554 RID: 1364 RVA: 0x0001CB9E File Offset: 0x0001AD9E
        public ILRegRuntimeLicense()
        {
        }

        // Token: 0x06000555 RID: 1365 RVA: 0x0001F710 File Offset: 0x0001D910
        internal ILRegRuntimeLicense(string licenseTypeName, string assemblysimplename, DateTime from, DateTime to, Version versionFrom, Version versionTo, bool isEvaluation, string machineKey) : base(licenseTypeName, assemblysimplename, from, to, versionFrom, versionTo, isEvaluation)
        {
            MachineKey = machineKey;
        }

        // Token: 0x06000556 RID: 1366 RVA: 0x0001F72B File Offset: 0x0001D92B
        protected override void WriteMembers(XmlWriter xml)
        {
            base.WriteMembers(xml);
            xml.WriteElementString(MachineKeyTagName, MachineKey);
        }

        // Token: 0x06000557 RID: 1367 RVA: 0x0001F748 File Offset: 0x0001D948
        internal override bool LoadXml(XmlDocument xml)
        {
            bool result = base.LoadXml(xml);
            MachineKey = xml.SelectSingleNode("//" + MachineKeyTagName).InnerText;
            if (MachineKey != LicenseManager.GetMachineHash())
            {
                result = false;
            }
            return result;
        }

        // Token: 0x06000558 RID: 1368 RVA: 0x0001F794 File Offset: 0x0001D994
        public override bool Equals(object obj)
        {
            ILRegRuntimeLicense ilregRuntimeLicense = obj as ILRegRuntimeLicense;
            return ilregRuntimeLicense != null && ilregRuntimeLicense.MachineKey == MachineKey;
        }

        // Token: 0x06000559 RID: 1369 RVA: 0x0001F7BE File Offset: 0x0001D9BE
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ MachineKey.GetHashCode();
        }

        // Token: 0x040000FF RID: 255
        private static readonly string MachineKeyTagName = "MachineKey";
    }
}
