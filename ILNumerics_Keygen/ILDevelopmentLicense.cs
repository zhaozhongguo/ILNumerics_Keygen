﻿using System;
using System.Globalization;
using System.Security;
using System.Xml;

namespace ILNumerics.Licensing
{
    /// <summary>
    ///       Allows the creation of AsmExecutionLicense instances. Installed into / loaded from local registry.
    ///       <para>[ILNumerics Core Module]</para>
    /// </summary>
    /// <remarks>
    ///   <para>[ILNumerics Core Module]</para>
    /// </remarks>
    // Token: 0x02000034 RID: 52
    [SecuritySafeCritical]
    [Serializable]
    public class ILDevelopmentLicense : ILRegRuntimeLicense
    {
        /// <summary>
        ///       controls properties for the resulting AsmRuntimeLicense
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000068 RID: 104
        // (get) Token: 0x060004F6 RID: 1270 RVA: 0x0001D561 File Offset: 0x0001B761
        // (set) Token: 0x060004F7 RID: 1271 RVA: 0x0001D569 File Offset: 0x0001B769
        public DateTime TargetFrom { get; set; }

        /// <summary>
        ///       controls properties for the resulting AsmRuntimeLicense
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000069 RID: 105
        // (get) Token: 0x060004F8 RID: 1272 RVA: 0x0001D572 File Offset: 0x0001B772
        // (set) Token: 0x060004F9 RID: 1273 RVA: 0x0001D57A File Offset: 0x0001B77A
        public DateTime TargetTo { get; set; }

        /// <summary>
        ///       Create empty development license (used for serialization)
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x060004FA RID: 1274 RVA: 0x0001D583 File Offset: 0x0001B783
        public ILDevelopmentLicense()
        {
        }

        // Token: 0x060004FB RID: 1275 RVA: 0x0001D58C File Offset: 0x0001B78C
        internal ILDevelopmentLicense(string licenseType, string assemblySimpleName, DateTime from, DateTime to, Version versionFrom, Version versionTo, bool isEvaluation, string machineKey, DateTime targetFrom, DateTime targetTo) : base(licenseType, assemblySimpleName, from, to, versionFrom, versionTo, isEvaluation, machineKey)
        {
            TargetFrom = targetFrom;
            TargetTo = targetTo;
        }

        // Token: 0x060004FC RID: 1276 RVA: 0x0001D5BC File Offset: 0x0001B7BC
        protected override void WriteMembers(XmlWriter xml)
        {
            base.WriteMembers(xml);
            xml.WriteElementString(TargetFromTagName, TargetFrom.ToString("o"));
            xml.WriteElementString(TargetToTagName, TargetTo.ToString("o"));
        }

        // Token: 0x060004FD RID: 1277 RVA: 0x0001D60C File Offset: 0x0001B80C
        internal override bool LoadXml(XmlDocument xml)
        {
            bool result = base.LoadXml(xml);
            TargetFrom = DateTime.Parse(xml.SelectSingleNode("//TargetFrom").InnerText, null, DateTimeStyles.RoundtripKind);
            TargetTo = DateTime.Parse(xml.SelectSingleNode("//TargetTo").InnerText, null, DateTimeStyles.RoundtripKind);
            return result;
        }

        // Token: 0x060004FE RID: 1278 RVA: 0x0001D664 File Offset: 0x0001B864
        public override bool Equals(object obj)
        {
            ILDevelopmentLicense ildevelopmentLicense = obj as ILDevelopmentLicense;
            return ildevelopmentLicense != null && ildevelopmentLicense.TargetFrom == TargetFrom && ildevelopmentLicense.TargetTo == TargetTo;
        }

        // Token: 0x060004FF RID: 1279 RVA: 0x0001D6A4 File Offset: 0x0001B8A4
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ TargetFrom.GetHashCode() ^ TargetTo.GetHashCode();
        }

        // Token: 0x040000D2 RID: 210
        private static readonly string TargetFromTagName = "TargetFrom";

        // Token: 0x040000D3 RID: 211
        private static readonly string TargetToTagName = "TargetTo";
    }
}
