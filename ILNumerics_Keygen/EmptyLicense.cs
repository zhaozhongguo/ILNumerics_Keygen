﻿using System.Security;

namespace ILNumerics.Licensing
{
    // Token: 0x02000035 RID: 53
    [SecuritySafeCritical]
    internal class EmptyLicense : ILLicense
    {
        // Token: 0x1700006A RID: 106
        // (get) Token: 0x06000501 RID: 1281 RVA: 0x0001D6EB File Offset: 0x0001B8EB
        public override string LicenseKey
        {
            get
            {
                return string.Empty;
            }
        }

        // Token: 0x06000502 RID: 1282 RVA: 0x000024D3 File Offset: 0x000006D3
        public override void Dispose()
        {
        }
    }
}