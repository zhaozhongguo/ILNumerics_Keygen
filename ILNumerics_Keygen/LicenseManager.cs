﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Xml;
using Microsoft.Win32;

namespace ILNumerics.Licensing
{
    // Token: 0x02000037 RID: 55
    [SecuritySafeCritical]
    internal class LicenseManager
    {
        /// <summary>
        ///       Singleton instance of the license manager
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000075 RID: 117
        // (get) Token: 0x06000525 RID: 1317 RVA: 0x0001DFF1 File Offset: 0x0001C1F1
        internal static LicenseManager Current
        {
            get
            {
                return m_instance;
            }
        }

        // Token: 0x06000526 RID: 1318 RVA: 0x0001DFF8 File Offset: 0x0001C1F8
        internal void RegisterType(Type type)
        {
            if (!licensedTypes.Contains(type))
            {
                licensedTypes.Add(type);
            }
        }

        // Token: 0x06000527 RID: 1319 RVA: 0x0001E015 File Offset: 0x0001C215
        internal bool IsRegistered(Type type)
        {
            return licensedTypes.Contains(type);
        }

        // Token: 0x06000528 RID: 1320 RVA: 0x0001E024 File Offset: 0x0001C224
        internal static string GetLicenseEmail()
        {
            using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\"))
            {
                if (registryKey != null && registryKey.GetValueNames().Contains("EmailAddress"))
                {
                    return registryKey.GetValue("EmailAddress").ToString();
                }
            }
            return "info@ilnumerics.net";
        }

        // Token: 0x17000076 RID: 118
        // (get) Token: 0x06000529 RID: 1321 RVA: 0x0001E08C File Offset: 0x0001C28C
        internal static bool AppDomainIsVerified
        {
            get
            {
                if (loaded == null)
                {
                    VerifyAssemblies();
                }
                return true;
            }
        }

        /// <summary>
        ///       Retrieves a collection of licenses for all modules registered on the system.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <returns>Collection of available licenses</returns>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x0600052A RID: 1322 RVA: 0x0001E0A0 File Offset: 0x0001C2A0
        internal static IEnumerable<T> GetLicenses<T>(string root = DefaultRegLicKeyLocationShared) where T : ILLicense
        {
            string[] array = typeof(T).Name.Split(new[]
            {
                '.'
            });
            string clsName = array[array.Length-1];
            IEnumerable<string> licensesStringsFromRegistry = GetLicensesStringsFromRegistry(root);
            if (licensesStringsFromRegistry == null) return null;
            var results=new List<T>();
            foreach (var item in licensesStringsFromRegistry)
            {
                if (item.Contains(clsName))
                {
                    var result = ILLicense.Create(item) as T;
                    if(result==null) continue;
                    results.Add(result);
                }
            }
            return results;
        }

        // Token: 0x0600052B RID: 1323 RVA: 0x0001E14C File Offset: 0x0001C34C
        internal static IEnumerable<T> GetLicenseForType<T>(Type type) where T : ILLicense
        {
            return from l in GetLicenses<T>()
                   where l.TargetTypeName == type.FullName
                   select l;
        }

        // Token: 0x0600052C RID: 1324 RVA: 0x0001E184 File Offset: 0x0001C384
        [SecuritySafeCritical]
        [PermissionSet(SecurityAction.Assert, Unrestricted = true)]
        internal static string GetMachineHash()
        {
            HashAlgorithm hashAlgorithm = SHA1.Create();
            string s = Environment.MachineName.ToLower() + Environment.ProcessorCount + Environment.UserName.ToLower();
            return Convert.ToBase64String(hashAlgorithm.ComputeHash(Encoding.Unicode.GetBytes(s)));
        }

        // Token: 0x0600052D RID: 1325 RVA: 0x0001E1D0 File Offset: 0x0001C3D0
        internal static void VerifyAssemblies()
        {
            AppDomain.CurrentDomain.AssemblyLoad += delegate (object _s, AssemblyLoadEventArgs _a)
            {
                VerifyAssembly(_a.LoadedAssembly);
            };
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!assembly.IsDynamic)
                {
                    VerifyAssembly(assembly);
                }
            }
            loaded = false;
        }

        // Token: 0x0600052E RID: 1326 RVA: 0x0001E23C File Offset: 0x0001C43C
        [FileIOPermission(SecurityAction.Assert, Unrestricted = true)]
        internal static void VerifyAssembly(Assembly assembly)
        {
            if (m_assemblyCache.ContainsKey(assembly.FullName) && m_assemblyCache[assembly.FullName])
            {
                return;
            }
            if (assembly.IsDynamic || string.IsNullOrEmpty(assembly.Location))
            {
                return;
            }
            AssemblyName[] array = (from r in assembly.GetReferencedAssemblies()
                                    where r.Name.StartsWith("ILNumerics.")
                                    select r).ToArray();
            List<AssemblyName> list = array.ToList();
            if (array.Length == 0)
            {
                return;
            }
            IEnumerable<ILAsmRuntimeLicense> source;
            if (GetLicensesFromAssembly(assembly, out source))
            {
                AssemblyName[] array2 = array;
                for (int i = 0; i < array2.Length; i++)
                {
                    AssemblyName reference = array2[i];
                    if (!source.Any(al => al.TargetAssemblySimpleName == reference.Name && al.MinVersion <= reference.Version && al.MaxVersion >= reference.Version && al.From <= DateTime.Now && al.To >= DateTime.Now))
                    {
                        break;
                    }
                    list.Remove(reference);
                }
            }
            if (list.Count() > 0)
            {
                string arg = string.Join("\r\n", from m in list
                                                 select m.FullName);
                throw new LicenseException(typeof(object), assembly, string.Format("A valid license for the following ILNumerics component(s) could not be found for the assembly '{0}'! Make sure that the assembly was built on a system which has a valid license for the components installed! Otherwise remove the reference(s) to the component(s) from the assembly and rebuild it!\r\n{1}", assembly.FullName, arg));
            }
            m_assemblyCache[assembly.FullName] = true;
        }

        // Token: 0x17000077 RID: 119
        // (get) Token: 0x0600052F RID: 1327 RVA: 0x0001E384 File Offset: 0x0001C584
        internal static bool IsILNumericsInstalled
        {
            get
            {
                try
                {
                    using (RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry32))
                    {
                        if (registryKey != null)
                        {
                            using (RegistryKey registryKey2 = registryKey.OpenSubKey("Software\\ILNumerics\\ILNumerics_Ultimate_VS\\"))
                            {
                                if (registryKey2 != null)
                                {
                                    object value = registryKey2.GetValue("LicenseKey");
                                    if (!Equals(value, null) && value.ToString().Length > 0)
                                    {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (SecurityException)
                {
                }
                return false;
            }
        }

        // Token: 0x06000530 RID: 1328 RVA: 0x0001E434 File Offset: 0x0001C634
        internal static bool GetLicensesFromAssembly(Assembly assembly, out IEnumerable<ILAsmRuntimeLicense> licenses)
        {
            bool result;
            try
            {
                string licName = ILNumericsLicenseResourceName.ToLower();
                string[] manifestResourceNames = assembly.GetManifestResourceNames();
                if (manifestResourceNames != null && manifestResourceNames.Length != 0)
                {
                    IEnumerable<string> source = from rn in manifestResourceNames
                                                 where rn.ToLower().Equals(licName)
                                                 select rn;
                    if (source.Count() == 1)
                    {
                        licName = source.First();
                    }
                    else
                    {
                        source = from rn in manifestResourceNames
                                 where rn.ToLower().EndsWith(licName)
                                 select rn;
                        if (source.Count() == 1)
                        {
                            licName = source.First();
                        }
                        else
                        {
                            licName = manifestResourceNames.FirstOrDefault(rn => rn.ToLower().Contains(licName));
                        }
                    }
                    using (Stream manifestResourceStream = assembly.GetManifestResourceStream(licName))
                    {
                        if (manifestResourceStream != null)
                        {
                            object[] array = new BinaryFormatter().Deserialize(manifestResourceStream) as object[];
                            if (array != null && array.Length == 2 && array[0] is string && array[1] is Hashtable)
                            {
                                Hashtable hashtable = array[1] as Hashtable;
                                List<ILAsmRuntimeLicense> list = new List<ILAsmRuntimeLicense>();
                                licenses = list;
                                foreach (object obj in hashtable.Values)
                                {
                                    ILAsmRuntimeLicense ilasmRuntimeLicense = ILLicense.Create<ILAsmRuntimeLicense>(obj.ToString(), assembly);
                                    if (ilasmRuntimeLicense != null)
                                    {
                                        list.Add(ilasmRuntimeLicense);
                                    }
                                }
                                return list.Count > 0;
                            }
                        }
                    }
                }
                licenses = null;
                result = false;
            }
            catch (Exception innerException)
            {
                throw new LicenseException(null, null, "Error retrieving embedded licenses from assembly resources. Assembly: " + assembly, innerException);
            }
            return result;
        }

        // Token: 0x06000531 RID: 1329 RVA: 0x0001E614 File Offset: 0x0001C814
        private static string GetOSVerionsString()
        {
            using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"))
            {
                if (registryKey != null)
                {
                    object value = registryKey.GetValue("CurrentVersion");
                    if (value != null)
                    {
                        return value.ToString();
                    }
                }
                foreach (string name in from o in registryKey.GetValueNames()
                                        orderby o.ToString()
                                        select o)
                {
                    object value2 = registryKey.GetValue(name);
                    if (value2 != null)
                    {
                        return value2.ToString();
                    }
                }
            }
            return string.Empty;
        }

        // Token: 0x06000532 RID: 1330 RVA: 0x0001E6E0 File Offset: 0x0001C8E0
        internal static IEnumerable<string> GetLicensesStringsFromRegistry(string root= DefaultRegLicKeyLocationShared)
        {
            IEnumerable<string> result;
            try
            {
                List<string> list = new List<string>();
                using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey(root))
                {
                    if (registryKey != null)
                    {
                        foreach (string name in registryKey.GetValueNames())
                        {
                            object value = registryKey.GetValue(name);
                            if (!Equals(value, null))
                            {
                                list.Add(value.ToString());
                            }
                        }
                    }
                }
                result = list;
            }
            catch (SecurityException)
            {
                result = null;
            }
            catch (UnauthorizedAccessException)
            {
                result = null;
            }
            catch (IOException)
            {
                result = null;
            }
            return result;
        }

        // Token: 0x06000533 RID: 1331 RVA: 0x0001E798 File Offset: 0x0001C998
        internal static void SetLicenses(string licenseKeysB64, string regRoot = DefaultRegLicKeyLocationShared)
        {
            if (licenseKeysB64 == null)
            {
                throw new ArgumentNullException("licenseKeysB64");
            }
            licenseKeysB64 = licenseKeysB64.Trim();
            if (licenseKeysB64.StartsWith("<?xml"))
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(licenseKeysB64);
                SetLicenses(from XmlNode n in xmlDocument.SelectNodes("//License")
                                           select Tuple.Create(Guid.Parse(n.Attributes["GUID"].Value), Encoding.Unicode.GetString(Convert.FromBase64String(n.InnerText))), regRoot);
                return;
            }
            SetLicenses(from k in licenseKeysB64.Split(new[]
            {
                ILLicense.s_sep
            }, StringSplitOptions.RemoveEmptyEntries)
                                       select Encoding.Unicode.GetString(Convert.FromBase64String(k)), regRoot);
        }

        // Token: 0x06000534 RID: 1332 RVA: 0x0001E850 File Offset: 0x0001CA50
        internal static void SetLicenses(IEnumerable<string> licenseKeys, string regRoot = DefaultRegLicKeyLocationShared)
        {
            GetLicenses<ILRegRuntimeLicense>().Concat(GetLicenses<ILDevelopmentLicense>());
            IEnumerable<string> enumerable = null;
            try
            {
                using (RegistryKey registryKey = Registry.CurrentUser.CreateSubKey(regRoot, RegistryKeyPermissionCheck.ReadWriteSubTree))
                {
                    enumerable = registryKey.GetValueNames();
                    foreach (string value in licenseKeys)
                    {
                        registryKey.SetValue(Guid.NewGuid().ToString(), value);
                    }
                    foreach (string name in enumerable)
                    {
                        registryKey.DeleteValue(name);
                    }
                }
            }
            catch (SecurityException ex)
            {
                Trace.WriteLine("License Management: Error storing licenses. Read details below.");
                Trace.WriteLine(ex.ToString());
            }
            catch (IOException ex2)
            {
                Trace.WriteLine("License Management: Error storing licenses. Read details below.");
                Trace.WriteLine(ex2.ToString());
            }
        }

        // Token: 0x06000535 RID: 1333 RVA: 0x0001E974 File Offset: 0x0001CB74
        internal static void SetLicenses(IEnumerable<Tuple<Guid, string>> licenseKeys, string regRoot = DefaultRegLicKeyLocationShared)
        {
            IEnumerable<string> enumerable = null;
            int num = 0;
            try
            {
                using (RegistryKey registryKey = Registry.CurrentUser.CreateSubKey(regRoot, RegistryKeyPermissionCheck.ReadWriteSubTree))
                {
                    enumerable = registryKey.GetValueNames().ToList();
                    foreach (Tuple<Guid, string> tuple in licenseKeys)
                    {
                        string text = tuple.Item1.ToString().Trim();
                        string value = tuple.Item2.Trim();
                        if (!string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(value))
                        {
                            registryKey.SetValue(text, value);
                            num++;
                        }
                    }
                    enumerable = enumerable.Except(from lk in licenseKeys
                                                   select lk.Item1.ToString());
                    foreach (string name in enumerable)
                    {
                        registryKey.DeleteValue(name);
                    }
                }
            }
            catch (SecurityException ex)
            {
                Trace.WriteLine("License Management: Error storing licenses. Read details below.");
                Trace.WriteLine(ex.ToString());
            }
            catch (IOException ex2)
            {
                Trace.WriteLine("License Management: Error storing licenses. Read details below.");
                Trace.WriteLine(ex2.ToString());
            }
            if (num == 0)
            {
                throw new ArgumentException("There are no modules associated with this license or the activation of this seat failed for unknown reasons.");
            }
        }

        // Token: 0x040000EE RID: 238
        internal const string DefaultRegLicKeyLocationBase = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\";

        // Token: 0x040000EF RID: 239
        internal const string DefaultRegLicKeyLocationShared = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\Licenses\\";

        // Token: 0x040000F0 RID: 240
        internal const string DefaultRegInstallDirKeyLocation32Bit = "Software\\ILNumerics\\ILNumerics Ultimate VS";

        // Token: 0x040000F1 RID: 241
        private const string ILNumericsLicenseResourceName = "ilnumerics.lic";

        // Token: 0x040000F2 RID: 242
        private static bool? loaded;

        // Token: 0x040000F3 RID: 243
        internal static readonly IList<Tuple<string, string>> LicensedModules = new List<Tuple<string, string>>
        {
            Tuple.Create("ILNumerics.ILMath", "Computing Engine"),
            Tuple.Create("ILNumerics.Drawing.ILScene", "Visualization Engine v4"),
            Tuple.Create("ILNumerics.Drawing.Scene", "Visualization Engine"),
            Tuple.Create("ILNumerics.IO.HDF5.H5File", "HDF5 API"),
            Tuple.Create("ILNumerics.VSExtension.VSVersion", "Visual Studio Extension"),
            Tuple.Create("ILNumerics.Toolboxes.Optimization", "Optimization Toolbox"),
            Tuple.Create("ILNumerics.Toolboxes.Statistics", "Statistics Toolbox"),
            Tuple.Create("ILNumerics.Toolboxes.MachineLearning", "Machine Learning Toolbox"),
            Tuple.Create("ILNumerics.Toolboxes.Interpolation", "Interpolation Toolbox"),
            Tuple.Create("ILNumerics.Drawing.Plotting.ILFastSurface", "Drawing Extensions v4"),
            Tuple.Create("ILNumerics.Drawing.Plotting.FastSurface", "Drawing Extensions Toolbox"),
            Tuple.Create("ILNumerics.numpy", "numpy API")
        };

        // Token: 0x040000F4 RID: 244
        private readonly HashSet<Type> licensedTypes = new HashSet<Type>();

        // Token: 0x040000F5 RID: 245
        private static readonly object s_typesCacheSyncObject = new object();

        // Token: 0x040000F6 RID: 246
        internal static readonly string LicenseDefaultWebSVCUri = "http://ilnumerics.net/licsvc.php";

        // Token: 0x040000F7 RID: 247
        private static readonly LicenseManager m_instance = new LicenseManager();

        // Token: 0x040000F8 RID: 248
        private object m_lockObject = new object();

        // Token: 0x040000F9 RID: 249
        private static readonly ConcurrentDictionary<string, bool> m_assemblyCache = new ConcurrentDictionary<string, bool>();
    }
}
