﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace ILNumerics.Licensing
{
    /// <summary>
    ///       Allows the execution of types. Embedded and loaded from licensed assemblies, restrict execution to that assembly (and not to an individual machine).
    ///       <para>[ILNumerics Core Module]</para>
    /// </summary>
    /// <remarks>
    ///   <para>[ILNumerics Core Module]</para>
    /// </remarks>
    // Token: 0x02000032 RID: 50
    [SecuritySafeCritical]
    [Serializable]
    internal class ILAsmRuntimeLicense : ILLicense
    {
        /// <summary>
        ///       Reference to the assembly given by AssemblyName or null (matching all assemblies).
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000063 RID: 99
        // (get) Token: 0x060004DA RID: 1242 RVA: 0x0001CB6B File Offset: 0x0001AD6B
        // (set) Token: 0x060004DB RID: 1243 RVA: 0x0001CB73 File Offset: 0x0001AD73
        internal Assembly Assembly { get; set; }

        /// <summary>
        ///       Fully qualified name or '*' (matching all assemblies).
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000064 RID: 100
        // (get) Token: 0x060004DC RID: 1244 RVA: 0x0001CB7C File Offset: 0x0001AD7C
        // (set) Token: 0x060004DD RID: 1245 RVA: 0x0001CB84 File Offset: 0x0001AD84
        internal string AssemblyName { get; set; }

        /// <summary>
        ///       Unique ID of the license used to create this asm license.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000065 RID: 101
        // (get) Token: 0x060004DE RID: 1246 RVA: 0x0001CB8D File Offset: 0x0001AD8D
        // (set) Token: 0x060004DF RID: 1247 RVA: 0x0001CB95 File Offset: 0x0001AD95
        internal Guid CreatedBy { get; set; }

        /// <summary>
        ///       Create empty asm license (used for serialization)
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x060004E0 RID: 1248 RVA: 0x0001CB9E File Offset: 0x0001AD9E
        public ILAsmRuntimeLicense()
        {
        }

        // Token: 0x060004E1 RID: 1249 RVA: 0x0001CBA8 File Offset: 0x0001ADA8
        protected ILAsmRuntimeLicense(string licenseTypeName, string assemblyname, DateTime from, DateTime to, Version versionFrom, Version versionTo, bool isEvaluation, Guid createdBy, Assembly customAssembly, ILDevelopmentLicense devlic) : base(licenseTypeName, assemblyname, from, to, versionFrom, versionTo, isEvaluation)
        {
            From = devlic.TargetFrom;
            To = devlic.TargetTo;
            CreatedBy = devlic.ID;
            TargetTypeName = devlic.TargetTypeName;
            TargetAssemblySimpleName = devlic.TargetAssemblySimpleName;
            Assembly = customAssembly;
            AssemblyName = customAssembly.GetName().FullName;
        }

        // Token: 0x060004E2 RID: 1250 RVA: 0x0001CC24 File Offset: 0x0001AE24
        internal ILAsmRuntimeLicense(Assembly assembly, ILDevelopmentLicense devlic) : this(devlic.TargetTypeName, devlic.TargetAssemblySimpleName, devlic.TargetFrom, devlic.TargetTo, devlic.MinVersion, devlic.MaxVersion, devlic.IsEvaluation, devlic.ID, assembly, devlic)
        {
        }

        // Token: 0x060004E3 RID: 1251 RVA: 0x0001CC6C File Offset: 0x0001AE6C
        protected override void WriteMembers(XmlWriter xml)
        {
            base.WriteMembers(xml);
            xml.WriteElementString(AssemblyNameTagName, AssemblyName);
            xml.WriteElementString(CreatedByTagName, CreatedBy.ToString("D"));
        }

        // Token: 0x060004E4 RID: 1252 RVA: 0x0001CCB0 File Offset: 0x0001AEB0
        internal override bool LoadXml(XmlDocument xml)
        {
            bool result = base.LoadXml(xml);
            CreatedBy = Guid.Parse(xml.SelectSingleNode("//" + CreatedByTagName).InnerText);
            AssemblyName = xml.SelectSingleNode("//" + AssemblyNameTagName).InnerText;
            return result;
        }

        // Token: 0x060004E5 RID: 1253 RVA: 0x0001CD0C File Offset: 0x0001AF0C
        internal static string GetHash(Assembly ass)
        {
            byte[] array = null;
            return GetHash(ass, ref array);
        }

        // Token: 0x060004E6 RID: 1254 RVA: 0x0001CD24 File Offset: 0x0001AF24
        internal static string GetHash(Assembly ass, ref byte[] bytes)
        {
            object obj = s_hashesCacheSyncObject;
            string result;
            lock (obj)
            {
                if (s_hashesCache.ContainsKey(ass))
                {
                    Tuple<string, byte[]> tuple = s_hashesCache[ass];
                    bytes = tuple.Item2;
                    result = tuple.Item1;
                }
                else
                {
                    SHA1 sha = SHA1.Create();
                    List<byte> list = new List<byte>(1000);
                    foreach (Module module in from mod in ass.GetModules()
                                              orderby mod.ScopeName
                                              select mod)
                    {
                        foreach (Type type in module.GetTypes().OrderBy(typ => typ.FullName))
                        {
                            foreach (MethodInfo methodInfo in type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.Public).OrderBy(met => met.ToString()))
                            {
                                hash(methodInfo.GetMethodBody(), sha, list);
                            }
                            foreach (MethodInfo methodInfo2 in type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.NonPublic).OrderBy(met => met.ToString()))
                            {
                                hash(methodInfo2.GetMethodBody(), sha, list);
                            }
                            foreach (MethodInfo methodInfo3 in type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public).OrderBy(met => met.ToString()))
                            {
                                hash(methodInfo3.GetMethodBody(), sha, list);
                            }
                            foreach (MethodInfo methodInfo4 in type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic).OrderBy(met => met.ToString()))
                            {
                                hash(methodInfo4.GetMethodBody(), sha, list);
                            }
                            foreach (ConstructorInfo constructorInfo in type.GetConstructors(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic).OrderBy(met => met.ToString()))
                            {
                                hash(constructorInfo.GetMethodBody(), sha, list);
                            }
                        }
                    }
                    bytes = sha.ComputeHash(list.ToArray());
                    string text = Convert.ToBase64String(bytes);
                    s_hashesCache.Add(ass, Tuple.Create(text, bytes));
                    result = text;
                }
            }
            return result;
        }

        // Token: 0x060004E7 RID: 1255 RVA: 0x0001D114 File Offset: 0x0001B314
        private static void hash(MethodBody methodBody, HashAlgorithm hashAlg, List<byte> hash)
        {
            if (methodBody == null)
            {
                return;
            }
            byte[] ilasByteArray = methodBody.GetILAsByteArray();
            hash.AddRange(hashAlg.ComputeHash(ilasByteArray));
        }

        // Token: 0x060004E8 RID: 1256 RVA: 0x0001D139 File Offset: 0x0001B339
        private static string getName(ParameterInfo[] parameterInfo)
        {
            if (parameterInfo == null || parameterInfo.Length == 0)
            {
                return string.Empty;
            }
            return string.Join("+", parameterInfo.Select(delegate (ParameterInfo parinf)
            {
                string text = string.Empty;
                if (!parinf.IsRetval)
                {
                    text += parinf.Position;
                    text += (string.IsNullOrEmpty(parinf.Name) ? "(no name)" : parinf.Name);
                    text += (parinf.IsOut ? "OUT" : "");
                    text += (parinf.IsIn ? "IN" : "");
                    text += (parinf.ParameterType.IsByRef ? "REF" : "");
                }
                return text;
            }));
        }

        // Token: 0x060004E9 RID: 1257 RVA: 0x0001D178 File Offset: 0x0001B378
        protected override void Sign(StringBuilder sb)
        {
            string cont = sb.ToString();
            sb.Append(s_sep);
            sb.Append(MakeSig(cont, PublicKey));
        }

        protected  void SignWithCh(StringBuilder sb)
        {
            string cont = sb.ToString();
            sb.Append(s_sep);
            string codeHashFromAssembly = getCodeHashFromAssembly(Assembly);
            sb.Append(MakeSig(cont, PublicKey, codeHashFromAssembly));
        }

        public  string ComputeKeyWithCh()
        {
            StringBuilder stringBuilder = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("License");
            xmlWriter.WriteAttributeString(LicenseTypeTagName, GetType().Name);
            WriteMembers(xmlWriter);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            SignWithCh(stringBuilder);
            return stringBuilder.ToString();
        }

        // Token: 0x060004EA RID: 1258 RVA: 0x0001D1AC File Offset: 0x0001B3AC
        protected override bool Verify(string key, Assembly assembly = null)
        {
            if (assembly == null)
            {
                return false;
            }
            if (Assembly == null)
            {
                Assembly = assembly;
            }
            string[] array = LicenseKey.Split(new[]
            {
                s_sep
            }, StringSplitOptions.RemoveEmptyEntries);
            if (array.Length != 2)
            {
                return false;
            }
            string codeHashFromAssembly = getCodeHashFromAssembly(assembly);
            return MakeSig(array[0], key, codeHashFromAssembly) == array[1];
        }

        // Token: 0x060004EB RID: 1259 RVA: 0x0001D218 File Offset: 0x0001B418
        private string getCodeHashFromAssembly(Assembly assembly)
        {
            IEnumerable<Type> enumerable = from t in assembly.GetTypes()
                                           where t.Namespace != null && t.Namespace.EndsWith("ILNumerics_Licensing") && t.Name.StartsWith("ILLicHelper_") && t.Name.EndsWith("_") && t.GetCustomAttributes(typeof(LicenseProviderAttribute), false).Any()
                                           select t;
            if (enumerable != null && enumerable.Count() > 0)
            {
                IEnumerable<FieldInfo> enumerable2 = from f in enumerable.First().GetFields(BindingFlags.Static | BindingFlags.Public)
                                                     where f.IsLiteral && f.Name == "CH"
                                                     select f;
                if (enumerable2 != null && enumerable2.Count() > 0)
                {
                    return enumerable2.First().GetRawConstantValue().ToString();
                }
            }
            return "";
        }

        // Token: 0x060004EC RID: 1260 RVA: 0x0001D2B0 File Offset: 0x0001B4B0
        private string MakeSig(string cont, string key, string ch = null)
        {
            if (Assembly == null)
            {
                throw new InvalidProgramException("An abnormal program state was encountered: internal licensing error in ILNumerics component. If this problem remains, please contact support@ilnumerics.net");
            }
            byte[] array = null;
            if (!string.IsNullOrEmpty(ch))
            {
                array = Convert.FromBase64String(ch);
            }
            else
            {
                GetHash(Assembly, ref array);
            }
            HashAlgorithm hashAlgorithm = SHA1.Create();
            List<byte> list = new List<byte>(500);
            list.AddRange(Encoding.UTF8.GetBytes(GetType().Assembly.FullName));
            int num = key.IndexOf("<Modulus>") + 1;
            byte[] array2 = Encoding.ASCII.GetBytes(key.Substring(num + "<Modulus>".Length));
            foreach (byte b in array)
            {
                list.Add(array2[b % array2.Length]);
            }
            array2 = list.ToArray();
            string result = "";
            using (AesCryptoServiceProvider aesCryptoServiceProvider = new AesCryptoServiceProvider())
            {
                int num2 = array.Length / 2;
                aesCryptoServiceProvider.Key = hashAlgorithm.ComputeHash(array, 0, array.Length / 2 + 1).Concat(hashAlgorithm.ComputeHash(array, num2 - 1, num2)).Take(32).ToArray();
                aesCryptoServiceProvider.IV = hashAlgorithm.ComputeHash(array2).Take(16).ToArray();
                byte[] bytes = Encoding.ASCII.GetBytes(cont);
                result = Convert.ToBase64String(aesCryptoServiceProvider.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length));
            }
            return result;
        }

        // Token: 0x060004ED RID: 1261 RVA: 0x0001D434 File Offset: 0x0001B634
        public override bool Equals(object obj)
        {
            ILAsmRuntimeLicense ilasmRuntimeLicense = obj as ILAsmRuntimeLicense;
            return ilasmRuntimeLicense != null && (ilasmRuntimeLicense.AssemblyName == AssemblyName && ilasmRuntimeLicense.Assembly == Assembly) && ilasmRuntimeLicense.CreatedBy == CreatedBy;
        }

        // Token: 0x060004EE RID: 1262 RVA: 0x0001D488 File Offset: 0x0001B688
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ AssemblyName.GetHashCode() ^ Assembly.GetHashCode() ^ CreatedBy.GetHashCode();
        }

        // Token: 0x040000CC RID: 204
        private static readonly string AssemblyNameTagName = "AssemblyName";

        // Token: 0x040000CD RID: 205
        private static readonly string CreatedByTagName = "CreatedBy";

        // Token: 0x040000CE RID: 206
        private static readonly Dictionary<Assembly, Tuple<string, byte[]>> s_hashesCache = new Dictionary<Assembly, Tuple<string, byte[]>>();

        // Token: 0x040000CF RID: 207
        private static readonly object s_hashesCacheSyncObject = new object();
    }
}
