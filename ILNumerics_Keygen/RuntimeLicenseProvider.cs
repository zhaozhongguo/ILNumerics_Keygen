﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;

namespace ILNumerics.Licensing
{
    /// <summary>
    ///       Internal class. Don't rename this class! It would break compatibility with previous versions!
    ///       <para>[ILNumerics Core Module]</para>
    /// </summary>
    /// <remarks>
    ///   <para>[ILNumerics Core Module]</para>
    /// </remarks>
    // Token: 0x02000038 RID: 56
    [SecuritySafeCritical]
    public class RuntimeLicenseProvider : LicenseProvider
    {
        // Token: 0x06000539 RID: 1337 RVA: 0x0001EC80 File Offset: 0x0001CE80
        public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
        {
            if (context.UsageMode == LicenseUsageMode.Runtime)
            {
                try
                {
                    Trace.WriteLine(string.Format("ILNumerics runtime license request - Type: {0}, Instance: {1}", type, (instance != null) ? instance.ToString() : "null"));
                    IEnumerable<ILRegRuntimeLicense> licenseForType = LicenseManager.GetLicenseForType<ILRegRuntimeLicense>(type);
                    if (licenseForType.Count() == 0)
                    {
                        Trace.WriteLine("No ILNumerics installation found. ILNumerics components will require valid runtime licenses to be embedded into all related assemblies.");
                        if (LicenseManager.AppDomainIsVerified)
                        {
                            Trace.WriteLine("Verifying embedded licenses for assemblies...");
                        }
                        return ILLicense.Empty;
                    }
                    return licenseForType.First();
                }
                catch (LicenseException ex)
                {
                    Trace.WriteLine("No license found for type: {0}", type.ToString());
                    Trace.WriteLine("Reason: {0}", ex.ToString());
                    if (allowExceptions)
                    {
                        throw;
                    }
                }
                catch (Exception ex2)
                {
                    Trace.WriteLine("Licensing error: " + ex2);
                    if (allowExceptions)
                    {
                        throw;
                    }
                }
                return null;
            }
            string errorCode = "Success";
            try
            {
                if (LicenseManager.IsILNumericsInstalled)
                {
                    ILicenseContext licenseContext = context as ILicenseContext;
                    IEnumerable<ILDevelopmentLicense> enumerable = null;
                    if (licenseContext != null && !string.IsNullOrEmpty(licenseContext.RegistryRoot))
                    {
                        enumerable = LicenseManager.GetLicenses<ILDevelopmentLicense>(licenseContext.RegistryRoot);
                    }
                    else
                    {
                        enumerable = LicenseManager.GetLicenses<ILDevelopmentLicense>();
                    }
                    if (enumerable != null)
                    {
                        Assembly assembly = type.Assembly;
                        AssemblyName[] array = (from r in type.Assembly.GetReferencedAssemblies()
                                                where r.Name.StartsWith("ILNumerics.")
                                                select r).ToArray();
                        for (int i = 0; i < array.Length; i++)
                        {
                            AssemblyName r = array[i];
                            try
                            {
                                foreach (ILDevelopmentLicense ildevelopmentLicense in from devlic in enumerable
                                                                                      where devlic.TargetAssemblySimpleName.Equals(r.Name)
                                                                                      select devlic)
                                {
                                    try
                                    {
                                        Type targetTypeFor = getTargetTypeFor(ildevelopmentLicense);
                                        ILAsmRuntimeLicense ilasmRuntimeLicense = new ILAsmRuntimeLicense(assembly, ildevelopmentLicense);
                                        context.SetSavedLicenseKey(targetTypeFor, ilasmRuntimeLicense.LicenseKey);
                                        break;
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }
                            catch (InvalidOperationException)
                            {
                                errorCode = "NoLicense";
                                throw new LicenseException(type, instance, "ILNumerics Licensing: unable to find/embed runtime license for component: " + r + "\r\nThe build will succeed but the resulting assembly will not run on machines without ILNumerics installed.\r\nMake sure to compile the project on a machine having a development license of the component installed or remove the reference from your project and try again!");
                            }
                        }
                    }
                }
                else
                {
                    errorCode = "NoInstall";
                }
            }
            catch (Exception ex3)
            {
                LogError("ILNumerics Licensing: an error occured while creating runtime licenses for the assembly " + type.Assembly + "\r\nMake sure to have installed a complete set of valid ILNumerics licenses on this machine for all ILNumerics components (references) involved. Error information following below.");
                LogError(ex3.ToString());
                throw;
            }
            finally
            {
                writeLCResponse(type, errorCode);
            }
            return ILLicense.Empty;
        }

        // Token: 0x0600053A RID: 1338 RVA: 0x0001EF80 File Offset: 0x0001D180
        private static void writeLCResponse(Type type, string errorCode)
        {
            if (type == null || type.Assembly == null)
            {
                return;
            }
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            if (entryAssembly == null || !entryAssembly.GetName().Name.ToLower().StartsWith("lc"))
            {
                return;
            }
            if (Directory.Exists("ILNumerics_deploy"))
            {
                try
                {
                    string hash = ILAsmRuntimeLicense.GetHash(type.Assembly);
                    File.WriteAllText("ILNumerics_deploy\\ilnumerics.tmplic", errorCode + "|" + hash);
                }
                catch (UnauthorizedAccessException)
                {
                }
                catch (SecurityException)
                {
                }
                catch (IOException)
                {
                }
            }
        }

        // Token: 0x0600053B RID: 1339 RVA: 0x0001F034 File Offset: 0x0001D234
        internal static Type getTargetTypeFor(ILLicense dl)
        {
            Assembly assembly2 = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(assembly => assembly.GetName().Name == dl.TargetAssemblySimpleName);
            if (assembly2 == null)
            {
                assembly2 = Assembly.LoadWithPartialName(dl.TargetAssemblySimpleName);
            }
            if (!(assembly2 != null))
            {
                throw new InvalidOperationException(string.Format("Unable to load assembly '{0}' for creating assembly license embedded resource.", dl.TargetAssemblySimpleName));
            }
            Type type = assembly2.GetType(dl.TargetTypeName, false);
            if (type == null)
            {
                throw new InvalidOperationException(string.Format("Unable to retrieve type '{0}' for embedding into assembly resources.", dl.TargetTypeName));
            }
            return type;
        }

        // Token: 0x0600053C RID: 1340 RVA: 0x0001F0E0 File Offset: 0x0001D2E0
        internal static void LogError(string p)
        {
            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry(p, EventLogEntryType.Warning, 101, 1);
            }
        }

        // Token: 0x0600053D RID: 1341 RVA: 0x0001F12C File Offset: 0x0001D32C
        public static string GetLicenseEmail()
        {
            return LicenseManager.GetLicenseEmail();
        }

        // Token: 0x0600053E RID: 1342 RVA: 0x0001F134 File Offset: 0x0001D334
        public static string GetDevLicenseKey(Guid guid, DateTime from, DateTime to, string typeName, string assemblyName, DateTime targetFrom, DateTime targetTo, bool isEvaluation, Version minVersion, Version maxVersion, string machineKey)
        {
            return new ILDevelopmentLicense(typeName, assemblyName, from, to, minVersion, maxVersion, isEvaluation, machineKey, targetFrom, targetTo)
            {
                ID = guid
            }.LicenseKey;
        }

        // Token: 0x0600053F RID: 1343 RVA: 0x0001F163 File Offset: 0x0001D363
        public static string GetRegRTLicenseKey(Guid guid, DateTime from, DateTime to, string typeName, string assemblyName, bool isEvaluation, Version minVersion, Version maxVersion, string machineKey)
        {
            if (string.IsNullOrEmpty(machineKey))
            {
                machineKey = LicenseManager.GetMachineHash();
            }
            return new ILRegRuntimeLicense(typeName, assemblyName, from, to, minVersion, maxVersion, isEvaluation, machineKey)
            {
                ID = guid
            }.LicenseKey;
        }

        // Token: 0x06000540 RID: 1344 RVA: 0x0000DDCD File Offset: 0x0000BFCD
        public static string GetMachineHash()
        {
            return LicenseManager.GetMachineHash();
        }

        // Token: 0x06000541 RID: 1345 RVA: 0x0001F193 File Offset: 0x0001D393
        public static IEnumerable<ILLicense> GetLicenses()
        {
            return LicenseManager.GetLicenses<ILRegRuntimeLicense>().Concat(LicenseManager.GetLicenses<ILDevelopmentLicense>());
        }

        // Token: 0x06000542 RID: 1346 RVA: 0x0001F1AE File Offset: 0x0001D3AE
        public static void SetLicenses(string licenseKeysB64, string regRoot = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\Licenses\\")
        {
            LicenseManager.SetLicenses(licenseKeysB64, regRoot);
        }

        // Token: 0x06000543 RID: 1347 RVA: 0x0001F1B8 File Offset: 0x0001D3B8
        public static string GetModuleNameFromType(string typeName)
        {
            if (typeName == null)
            {
                throw new ArgumentNullException("invalid type name");
            }
            IEnumerable<Tuple<string, string>> enumerable = from m in LicenseManager.LicensedModules
                                                            where m.Item1 == typeName.Trim()
                                                            select m;
            if (enumerable != null && enumerable.Count() == 1)
            {
                return enumerable.First().Item2;
            }
            throw new NotSupportedException("unknown or unsupported licensed type");
        }

        // Token: 0x17000078 RID: 120
        // (get) Token: 0x06000544 RID: 1348 RVA: 0x0001F21E File Offset: 0x0001D41E
        public static IEnumerable<string> LicensedModulesFullNames
        {
            get
            {
                return from lm in LicenseManager.LicensedModules
                       select lm.Item1;
            }
        }

        // Token: 0x17000079 RID: 121
        // (get) Token: 0x06000545 RID: 1349 RVA: 0x0001F249 File Offset: 0x0001D449
        public static IEnumerable<string> LicensedModulesDescription
        {
            get
            {
                return from lm in LicenseManager.LicensedModules
                       select lm.Item2;
            }
        }

        // Token: 0x06000546 RID: 1350 RVA: 0x0001F274 File Offset: 0x0001D474
        public static void BeginRefreshLicenses(Action<LicensingSVCResponse> responseCallback, Guid guid, string email, Uri uri = null, string machineKey = null)
        {
            new LicSVCClient().BeginDownloadLicenses(responseCallback, guid, email, uri, null, machineKey);
        }

        // Token: 0x040000FA RID: 250
        private const string RegRoot = "Software\\ILNumerics\\ILNumerics_Ultimate_VS\\Licenses\\";
    }
}
