﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using System.Xml;
// ReSharper disable StringLiteralTypo

// ReSharper disable CommentTypo
// ReSharper disable IdentifierTypo
// ReSharper disable InconsistentNaming
// ReSharper disable RedundantTypeSpecificationInDefaultExpression
// ReSharper disable ConditionIsAlwaysTrueOrFalse
// ReSharper disable RedundantArgumentDefaultValue
// ReSharper disable RedundantExplicitArrayCreation

namespace ILNumerics.Licensing
{
    /// <summary>
    ///       Base class for all licenses
    ///       <para>[ILNumerics Core Module]</para>
    /// </summary>
    /// <remarks>
    ///   <para>[ILNumerics Core Module]</para>
    /// </remarks>
    // Token: 0x02000036 RID: 54
    [SecuritySafeCritical]
    public abstract class ILLicense : License
    {
        /// <summary>
        ///       Minimum version of the assembly hosting <see cref="P:ILNumerics.Licensing.ILLicense.TargetTypeName" /> which this license is able to unlock
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700006B RID: 107
        // (get) Token: 0x06000504 RID: 1284 RVA: 0x0001D6F2 File Offset: 0x0001B8F2
        // (set) Token: 0x06000505 RID: 1285 RVA: 0x0001D6FA File Offset: 0x0001B8FA
        internal Version MinVersion { get; set; }

        /// <summary>
        ///       Maximum version of the assembly hosting <see cref="P:ILNumerics.Licensing.ILLicense.TargetTypeName" /> which this license is able to unlock
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700006C RID: 108
        // (get) Token: 0x06000506 RID: 1286 RVA: 0x0001D703 File Offset: 0x0001B903
        // (set) Token: 0x06000507 RID: 1287 RVA: 0x0001D70B File Offset: 0x0001B90B
        internal Version MaxVersion { get; set; }

        /// <summary>
        ///       Start validity date of this license
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700006D RID: 109
        // (get) Token: 0x06000508 RID: 1288 RVA: 0x0001D714 File Offset: 0x0001B914
        // (set) Token: 0x06000509 RID: 1289 RVA: 0x0001D71C File Offset: 0x0001B91C
        public DateTime From { get; set; }

        /// <summary>
        ///       End validity date of this license, set to DateTime.MaxValue for perpetual licenses.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700006E RID: 110
        // (get) Token: 0x0600050A RID: 1290 RVA: 0x0001D725 File Offset: 0x0001B925
        // (set) Token: 0x0600050B RID: 1291 RVA: 0x0001D72D File Offset: 0x0001B92D
        public DateTime To { get; set; }

        /// <summary>
        ///       The type name this license is able to unlock.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x1700006F RID: 111
        // (get) Token: 0x0600050C RID: 1292 RVA: 0x0001D736 File Offset: 0x0001B936
        // (set) Token: 0x0600050D RID: 1293 RVA: 0x0001D73E File Offset: 0x0001B93E
        public string TargetTypeName { get; set; }

        /// <summary>
        ///       Assembly qualified name of the assembly hosting the licensed type
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000070 RID: 112
        // (get) Token: 0x0600050E RID: 1294 RVA: 0x0001D747 File Offset: 0x0001B947
        // (set) Token: 0x0600050F RID: 1295 RVA: 0x0001D74F File Offset: 0x0001B94F
        public string TargetAssemblySimpleName { get; set; }

        /// <summary>
        ///       Singleton of an empty license.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000071 RID: 113
        // (get) Token: 0x06000510 RID: 1296 RVA: 0x0001D758 File Offset: 0x0001B958
        public static License Empty
        {
            get
            {
                return s_empty;
            }
        }

        /// <summary>
        ///       Unique identifier for this license.
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000072 RID: 114
        // (get) Token: 0x06000511 RID: 1297 RVA: 0x0001D75F File Offset: 0x0001B95F
        // (set) Token: 0x06000512 RID: 1298 RVA: 0x0001D767 File Offset: 0x0001B967
        public Guid ID { get; set; }

        /// <summary>
        ///       Flag determining if this is a trial license
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x17000073 RID: 115
        // (get) Token: 0x06000513 RID: 1299 RVA: 0x0001D770 File Offset: 0x0001B970
        // (set) Token: 0x06000514 RID: 1300 RVA: 0x0001D778 File Offset: 0x0001B978
        public bool IsEvaluation { get; set; }

        /// <summary>
        ///       Create empty license (used for serialization)
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x06000515 RID: 1301 RVA: 0x0001D781 File Offset: 0x0001B981
        public ILLicense()
        {
        }

        // Token: 0x06000516 RID: 1302 RVA: 0x0001D7B0 File Offset: 0x0001B9B0
        protected ILLicense(string licenseTypeName, string assemblyname, DateTime from, DateTime to, Version versionFrom, Version versionTo, bool isEvaluation)
        {
            ID = Guid.NewGuid();
            TargetTypeName = licenseTypeName;
            TargetAssemblySimpleName = assemblyname;
            From = from;
            To = to;
            MinVersion = versionFrom;
            MaxVersion = versionTo;
            IsEvaluation = isEvaluation;
        }

        // Token: 0x06000517 RID: 1303 RVA: 0x000024D3 File Offset: 0x000006D3
        public override void Dispose()
        {
        }

        // Token: 0x17000074 RID: 116
        // (get) Token: 0x06000518 RID: 1304 RVA: 0x0001D829 File Offset: 0x0001BA29
        public override string LicenseKey
        {
            get
            {
                if (string.IsNullOrEmpty(m_key))
                {
                    m_key = ComputeKey();
                }
                return m_key;
            }
        }

        // Token: 0x06000519 RID: 1305 RVA: 0x0001D84C File Offset: 0x0001BA4C
        protected virtual string ComputeKey()
        {
            StringBuilder stringBuilder = new StringBuilder();
            XmlWriter xmlWriter = XmlWriter.Create(stringBuilder);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("License");
            xmlWriter.WriteAttributeString(LicenseTypeTagName, GetType().Name);
            WriteMembers(xmlWriter);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            Sign(stringBuilder);
            return stringBuilder.ToString();
        }

        /// <summary>
        ///       Sign the license content using ILNumerics private key
        ///       <para>[ILNumerics Core Module]</para>
        /// </summary>
        /// <param name="sb">string builder with complete license content</param>
        /// <remarks>
        ///   <para>[ILNumerics Core Module]</para>
        /// </remarks>
        // Token: 0x0600051A RID: 1306 RVA: 0x0001D8B4 File Offset: 0x0001BAB4
        protected virtual void Sign(StringBuilder sb)
        {
            string s = sb.ToString();
            string xmlString = probeForKey();
            if (!string.IsNullOrEmpty(xmlString))
            {
                RSACryptoServiceProvider rsacryptoServiceProvider = new RSACryptoServiceProvider();
                new KeyContainerPermission(KeyContainerPermissionFlags.AllFlags).Assert();
                rsacryptoServiceProvider.FromXmlString(xmlString);
                string value = Convert.ToBase64String(rsacryptoServiceProvider.SignData(Encoding.Unicode.GetBytes(s), new SHA1CryptoServiceProvider()));
                CodeAccessPermission.RevertAll();
                sb.Append(s_sep);
                sb.Append(value);
                return;
            }
            throw new InvalidOperationException("Unable to create the license: no crypto keys were found!");
        }

        private string probeForKey()
        {
            return PrivateKey;
        }

        // Token: 0x0600051B RID: 1307 RVA: 0x0001D940 File Offset: 0x0001BB40
        private string probeForKeyFile(string pvkFileName)
        {
            try
            {
                string text = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, pvkFileName);
                if (File.Exists(text))
                {
                    return text;
                }
            }
            catch (SecurityException)
            {
            }
            try
            {
                string text = Path.Combine(Environment.CurrentDirectory, pvkFileName);
                if (File.Exists(text))
                {
                    return text;
                }
            }
            catch (SecurityException)
            {
            }
            try
            {
                string text = Path.Combine(GetType().Assembly.Location, pvkFileName);
                if (File.Exists(text))
                {
                    return text;
                }
            }
            catch (SecurityException)
            {
            }
            return string.Empty;
        }

        // Token: 0x0600051C RID: 1308 RVA: 0x0001D9EC File Offset: 0x0001BBEC
        protected virtual bool Verify(string key, Assembly assembly = null)
        {
            RSACryptoServiceProvider rsacryptoServiceProvider = new RSACryptoServiceProvider();
            rsacryptoServiceProvider.FromXmlString(PublicKey);
            string[] array = LicenseKey.Split(new string[]
            {
                s_sep
            }, StringSplitOptions.None);
            string s = array[0];
            string s2 = array[1];
            return rsacryptoServiceProvider.VerifyData(Encoding.Unicode.GetBytes(s), new SHA1CryptoServiceProvider(), Convert.FromBase64String(s2));
        }

        // Token: 0x0600051D RID: 1309 RVA: 0x0001DA48 File Offset: 0x0001BC48
        protected virtual void WriteMembers(XmlWriter xml)
        {
            xml.WriteElementString(IDTagName, ID.ToString("D"));
            xml.WriteElementString(FromTagName, From.ToString("o"));
            xml.WriteElementString(ToTagName, To.ToString("o"));
            xml.WriteElementString(MinVersionTagName, MinVersion.ToString());
            xml.WriteElementString(MaxVersionTagName, MaxVersion.ToString());
            xml.WriteElementString(TargetTypeTagName, TargetTypeName);
            xml.WriteElementString(TargetAssemblyNameTagName, TargetAssemblySimpleName.ToString(s_cult));
            xml.WriteElementString(IsEvalutionTagName, IsEvaluation.ToString(s_cult));
        }

        // Token: 0x0600051E RID: 1310 RVA: 0x0001DB28 File Offset: 0x0001BD28
        internal static ILLicense Create(string l)
        {
            if (l == null)
            {
                return null;
            }
            if (l.Contains("ILRegRuntimeLicense"))
            {
                return Create<ILRegRuntimeLicense>(l, null);
            }
            if (l.Contains("ILAsmRuntimeLicense"))
            {
                return Create<ILAsmRuntimeLicense>(l, null);
            }
            if (l.Contains("ILDevelopmentLicense"))
            {
                return Create<ILDevelopmentLicense>(l, null);
            }
            return null;
        }

        internal static ILLicense CreateWithoutVerify(string l)
        {
            if (l == null)
            {
                return null;
            }
            if (l.Contains("ILRegRuntimeLicense"))
            {
                return Create<ILRegRuntimeLicense>(l);
            }
            if (l.Contains("ILAsmRuntimeLicense"))
            {
                return Create<ILAsmRuntimeLicense>(l);
            }
            if (l.Contains("ILDevelopmentLicense"))
            {
                return Create<ILDevelopmentLicense>(l);
            }
            return null;
        }

        // Token: 0x0600051F RID: 1311 RVA: 0x0001DB7C File Offset: 0x0001BD7C
        [PermissionSet(SecurityAction.Assert, Unrestricted = true)]
        public static T Create<T>(string savedLicenseKey, Assembly assembly = null) where T : ILLicense, new()
        {
            var t = Create<T>(savedLicenseKey);
            Version version = typeof(ILLicense).Assembly.GetName().Version;
            if (t.MinVersion > version || t.MaxVersion < version)
            {
                RuntimeLicenseProvider.LogError(string.Format("ILNumerics Licensing Error: the requested assembly version '{0}' is not allowed for the current license installed: '{1}' - '{2}'", version, t.MinVersion, t.MaxVersion));
                return default(T);
            }
            t.m_key = savedLicenseKey;
            if (!t.Verify(PublicKey, assembly))
            {
                RuntimeLicenseProvider.LogError("Error creating license. Unable to deserialize from license key. Used key follows below. \r\n" + savedLicenseKey);
                return default(T);
            }
            return t;
        }

        internal static T Create<T>(string savedLicenseKey) where T : ILLicense, new()
        {
            T t = Activator.CreateInstance<T>();
            string[] array = savedLicenseKey.Split(new string[]
            {
                s_sep
            }, StringSplitOptions.None);
            if (array == null || array.Length != 2)
            {
                T result = default(T);
                return result;
            }
            string xml = array[0];
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.LoadXml(xml);
            }
            catch (XmlException)
            {
                RuntimeLicenseProvider.LogError("Error creating license. Corrupted xml. Used key follows below. \r\n" + savedLicenseKey);
                T result = default(T);
                return result;
            }
            try
            {
                if (!t.LoadXml(xmlDocument))
                {
                    return default(T);
                }
            }
            catch (NullReferenceException)
            {
                RuntimeLicenseProvider.LogError("Error creating license. Unable to deserialize from license key. Used key follows below. \r\n" + savedLicenseKey);
                return default(T);
            }
            catch (FileNotFoundException)
            {
                RuntimeLicenseProvider.LogError("Error creating license. Unable to load a type which was expected to be found on the system. Affected key is listed below. \r\n" + savedLicenseKey);
                return default(T);
            }
            return t;
        }

        // Token: 0x06000520 RID: 1312 RVA: 0x0001DD30 File Offset: 0x0001BF30
        internal virtual bool LoadXml(XmlDocument xml)
        {
            if (xml == null)
            {
                return false;
            }
            bool flag = true;
            ID = Guid.Parse(xml.SelectSingleNode("//" + IDTagName).InnerText);
            From = DateTime.Parse(xml.SelectSingleNode("//" + FromTagName).InnerText, null, DateTimeStyles.RoundtripKind);
            flag &= (From <= DateTime.Now);
            To = DateTime.Parse(xml.SelectSingleNode("//" + ToTagName).InnerText, null, DateTimeStyles.RoundtripKind);
            flag &= (To >= DateTime.Now);
            TargetAssemblySimpleName = xml.SelectSingleNode("//" + TargetAssemblyNameTagName).InnerText;
            TargetTypeName = xml.SelectSingleNode("//" + TargetTypeTagName).InnerText;
            IsEvaluation = bool.Parse(xml.SelectSingleNode("//" + IsEvalutionTagName).InnerText);
            Version version = null;
            XmlNode xmlNode = xml.SelectSingleNode("//" + MinVersionTagName);
            if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
            {
                if (!Version.TryParse(xmlNode.InnerText, out version))
                {
                    return false;
                }
                MinVersion = version;
            }
            xmlNode = xml.SelectSingleNode("//" + MaxVersionTagName);
            if (xmlNode != null && !string.IsNullOrEmpty(xmlNode.InnerText))
            {
                if (!Version.TryParse(xmlNode.InnerText, out version))
                {
                    return false;
                }
                MaxVersion = version;
            }
            return flag;
        }

        // Token: 0x06000521 RID: 1313 RVA: 0x0001DEC9 File Offset: 0x0001C0C9
        private Assembly resolveAssembly(string assemblyName)
        {
            return AppDomain.CurrentDomain.Load(assemblyName);
        }

        // Token: 0x06000522 RID: 1314 RVA: 0x0001DED8 File Offset: 0x0001C0D8
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is ILLicense))
            {
                return false;
            }
            ILLicense illicense = obj as ILLicense;
            return illicense.From == From && illicense.To == To && illicense.TargetTypeName == TargetTypeName;
        }

        // Token: 0x06000523 RID: 1315 RVA: 0x0001DF34 File Offset: 0x0001C134
        public override int GetHashCode()
        {
            return From.GetHashCode() ^ To.GetHashCode() ^ TargetTypeName.GetHashCode();
        }

        // Token: 0x040000D6 RID: 214
        private static ILLicense s_empty = new EmptyLicense();

        // Token: 0x040000D7 RID: 215
        private static readonly string TargetTypeTagName = "TargetType";

        // Token: 0x040000D8 RID: 216
        private static readonly string TargetAssemblyNameTagName = "TargetAssemblyName";

        // Token: 0x040000D9 RID: 217
        private static readonly string FromTagName = "From";

        // Token: 0x040000DA RID: 218
        private static readonly string ToTagName = "To";

        // Token: 0x040000DB RID: 219
        protected static readonly string LicenseTypeTagName = "Type";

        // Token: 0x040000DC RID: 220
        private static readonly string IDTagName = "ID";

        // Token: 0x040000DD RID: 221
        private static readonly string IsEvalutionTagName = "IsEvaluation";

        // Token: 0x040000DE RID: 222
        private static readonly string MinVersionTagName = "MinVersion";

        // Token: 0x040000DF RID: 223
        private static readonly string MaxVersionTagName = "MaxVersion";

        // Token: 0x040000E0 RID: 224 原程序内置PublicKey
        internal static readonly string s_pubcont = "<RSAKeyValue><Modulus>vWGrfKWk3ikpdl6NII7Jp1LVskP8POQBHE2UBSlHdh4EC9/J4WT8F6B/t4+bD73LpLDGb1JxhWV5bO68nu/thBxPJCPH1EVczv5zTr3q10nQvk4IluyxoP0fI5yQQ0NVNk1jhVxfqqeye2vs9uTPW8dvEL6k0PRgTgUNCFInTHUuDDvAq4qKBM/IZ83fFIYZyOA9WVm+Cd7rAr8o5ixp53IyIoFPBvGOrOLk6QAjUp3zybUuTyL0TESY+u3T3hpDqC1GRlTpEdiquCxKXCafd1VcbhbEXfxEspf66TGKGrP+57D50+EBUP+TTtvvjDssvslGjRJ3dIqzoUxif+4dWw==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

        #region RSA Keys

        public const string PublicKey =
            "<RSAKeyValue><Modulus>s+7smgiYXFEZFLdapWek3cX6MTNZVhqNuXQoAl9ZAcP4JvVeb98wyYV8EKsqt/wXL4xnIyzQ4WdsFvC5wVt7tHocJ7cJeq5dvXyt0034XdtBN6Eqi3cadRDP8y9xyT2W8o70x8YxODbS7nWQX629vVJZpqxedZo0HhfeS2rPQ5Kj4qPpVqVJVsQNBAfPWusIWFFli4il9HwvonU3RvCK7ZpflUsXvRnjUL2vZG8zEqxsXIEHgEIuRARjggofbdXNA5dopIkCvcg6g8tR5BJdnp867HoaxoIa5+a8E9b2l1S9ov0e9G4e0pPA6fhKvZuDejHQFSDxmXW7Wv7/09QqbQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

        public const string PrivateKey =
            "<RSAKeyValue><Modulus>s+7smgiYXFEZFLdapWek3cX6MTNZVhqNuXQoAl9ZAcP4JvVeb98wyYV8EKsqt/wXL4xnIyzQ4WdsFvC5wVt7tHocJ7cJeq5dvXyt0034XdtBN6Eqi3cadRDP8y9xyT2W8o70x8YxODbS7nWQX629vVJZpqxedZo0HhfeS2rPQ5Kj4qPpVqVJVsQNBAfPWusIWFFli4il9HwvonU3RvCK7ZpflUsXvRnjUL2vZG8zEqxsXIEHgEIuRARjggofbdXNA5dopIkCvcg6g8tR5BJdnp867HoaxoIa5+a8E9b2l1S9ov0e9G4e0pPA6fhKvZuDejHQFSDxmXW7Wv7/09QqbQ==</Modulus><Exponent>AQAB</Exponent><P>1Oc8/haKPzX68xELxhZjXsh5xCIBpPTqe5ZXu7f+3q/yNoICwmd2sfd5bnu2h/iQVUq2ehrAaaOCe6kdghHm3Bt+4jSMyU5otC3kBz4FQAIZd8xA+KgMIZc2L/9wkb0F09Jr7QJA+8CLAH+HErdOWEEdSS8Er7XPPbDJlLSxwWc=</P><Q>2FsqA6Qt48q5yuzSPvOzq0ruGK9AooPj1JEweyjEcsrlJ+K9qjtcULcJ0sBmly5uBIRndQhtr1dz9gnVbL98wxyk7v7do63nePaZc8noHk/VXCFArI7DZ++9omHJt/Lqo4aQcczqY7WWYtwfAKV+KTL/PHeTAWYSm5W9X1lobQs=</Q><DP>Q8GH1rFjs/RA9kRRg+wyGcMhhXrxHQdY7JYPO821zjcelZxQtn/o9YB14+R5VdKUVKCfbELBQ51m9GYA3xlFaH1TpzfkKljK17TXIjrpvtIsOsUFVF1e5v3Rn/riX29RwHTyJg6WkchYNINhgGJYC5xFbroI+saYuuBddvb+rzc=</DP><DQ>BHhNs+mOcShdEk42QcqzKZTQun3Ubgcfi+K8B69d76k1ds9+b6fuTlSZxVroQ7NX3owFvys/aOH7qVemqyqBn15WIgHt/lW3dI5TuUqwmv+3PsIJHgeNDfasClDO5BgGtP4wucksBodX5g76LP8+TgeZslweE2dSdxhu4ytrtuU=</DQ><InverseQ>I8h8Q0UIC1T1CO0MNCQvyL0lIiUMe3Kd/kEUTBfbRXj1E63/D9UH805i5BMQ5IsIu+O867o1u2YlSbAMjwqyEP26b29zd1DXKpChBtjk6L1MwmIw6D6kqp+um96PaTcj/LX4kkqkCIFxrejXpyAmUoXsSuuhjoRTnrieAEsmYQE=</InverseQ><D>EKmehYEoQtxLu0xje4/Gh7cOvUGpEz0wRK1KCzYzMuOn9kugrDF5/h97ZrvDH/ibWQ7iejJrQYRnURSHzy0eno70ToU3voGpMqP6IOhGiMKGHpWxL+seqxqcd0O8gMtQUDQG3P0wj6kcDXMbpJCdUovuu22Y7cES9Hp3XnqMqRAilqItPJZQh4WxZru1/Zg0IsORotZjZ4apfGO4Nr4XZNZEJO+ierN3l+5bRyaaq6eaFjfICM6UTgiZjPjzynkUXpPcwV/4tEreyiWWMRrH/Z0UXKY7lMwPhG/aTm0N4fu6Op4pLQoqWHBK06QvdqqU4GVWWDgjnb11ITogtPXNLQ==</D></RSAKeyValue>";

        #endregion


        // Token: 0x040000E1 RID: 225
        protected readonly CultureInfo s_cult = CultureInfo.GetCultureInfo("en-us");

        // Token: 0x040000E2 RID: 226
        protected readonly string s_pvkFilePath = "ilnpvk.xml";

        // Token: 0x040000E3 RID: 227
        internal static readonly string s_sep = "=======2==========";

        // Token: 0x040000E4 RID: 228
        protected readonly string s_datatimeformat = "YYYY-mm-dd;hh:MM:ss";

        // Token: 0x040000E5 RID: 229
        protected string m_key;
    }
}
