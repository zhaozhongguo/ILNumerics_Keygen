﻿namespace ILNumerics.Licensing
{
    // Token: 0x02000031 RID: 49
    internal interface ILicenseContext
    {
        // Token: 0x17000062 RID: 98
        // (get) Token: 0x060004D9 RID: 1241
        string RegistryRoot { get; }
    }
}