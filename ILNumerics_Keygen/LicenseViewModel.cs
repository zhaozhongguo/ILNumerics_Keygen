﻿using System;

namespace ILNumerics.Licensing
{
    // Token: 0x02000003 RID: 3
    public class LicenseViewModel
    {
        // Token: 0x17000002 RID: 2
        // (get) Token: 0x0600001B RID: 27 RVA: 0x00003AE4 File Offset: 0x00001CE4
        // (set) Token: 0x0600001C RID: 28 RVA: 0x00003AEC File Offset: 0x00001CEC
        public string Module { get; set; }

        // Token: 0x17000003 RID: 3
        // (get) Token: 0x0600001D RID: 29 RVA: 0x00003AF5 File Offset: 0x00001CF5
        // (set) Token: 0x0600001E RID: 30 RVA: 0x00003AFD File Offset: 0x00001CFD
        public string Execution { get; set; }

        // Token: 0x17000004 RID: 4
        // (get) Token: 0x0600001F RID: 31 RVA: 0x00003B06 File Offset: 0x00001D06
        // (set) Token: 0x06000020 RID: 32 RVA: 0x00003B0E File Offset: 0x00001D0E
        public string Development { get; set; }

        // Token: 0x17000005 RID: 5
        // (get) Token: 0x06000021 RID: 33 RVA: 0x00003B17 File Offset: 0x00001D17
        // (set) Token: 0x06000022 RID: 34 RVA: 0x00003B1F File Offset: 0x00001D1F
        public string Distribution { get; set; }

        // Token: 0x17000006 RID: 6
        // (get) Token: 0x06000023 RID: 35 RVA: 0x00003B28 File Offset: 0x00001D28
        // (set) Token: 0x06000024 RID: 36 RVA: 0x00003B30 File Offset: 0x00001D30
        public DateTime To { get; set; }
    }
}